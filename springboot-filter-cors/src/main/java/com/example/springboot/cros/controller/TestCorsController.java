package com.example.springboot.cros.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * 1。 手动返回response
 * 2。 使用 @CrossOrigin
 *
 */
@RestController
public class TestCorsController {

    /**
     * 配置跨域
     * @param response
     * @return
     */
    @RequestMapping("/hello")
    public String index(HttpServletResponse response){
        response.addHeader("Access-Control-Allow-Origin", "http://localhost:8080");
        return "Hello World";
    }

    /**
     * 配置跨域,
     * @param response
     * @return
     */
    @RequestMapping("/hello")
    @CrossOrigin("http://localhost:8080")
    public String index2(HttpServletResponse response){
        return "Hello World";
    }
}
