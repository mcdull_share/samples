package com.example.springboot.cros.filter.cors;

/*
  跨域配置几种方式
  1。配置新的 CorsFilter 过滤器，实现filter 接口

  2。重写 WebMvcConfigurer

  3. 使用注解（`@CrossOrigin`） controller上使用（修饰在方法或类）

  4. 手动返回 HttpServletResponse
    @RequestMapping("/hello")
    @ResponseBody
    public String index(HttpServletResponse response){
        response.addHeader("Access-Control-Allow-Origin", "http://localhost:8080");
        return "Hello World";
    }
 */