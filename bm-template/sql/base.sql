-- 用户表
create table t_user
(
    ID               bigint auto_increment comment 'ID'
        primary key,
    LOGIN_NAME       varchar(50)  not null comment '登录名，手机号',
    USER_NAME        varchar(50)  not null comment '用户名',
    PASSWORD         varchar(128) not null comment '密码',
    EMAIL            varchar(128) null comment '邮箱',
    MOBILE           varchar(20)  null comment '联系电话',
    STATUS           int default 1 comment '状态 0锁定 1有效',
    SEX              int          null comment '性别 0男 1女 2保密',
    HEAD_ICON        varchar(100) null comment '头像',
    USER_TYPE        int          null comment '用户类型：1：总部管理员 2：分公司管理员 3：代理商管理员',
    DESCRIPTION      varchar(100) null comment '描述',
    DEPT_ID          varchar(32)  null comment '部门ID',
    LAST_LOGIN_TIME  datetime     null comment '最近访问时间',
    CREATE_USER      varchar(50)  null comment '创建用户',
    CREATE_TIME      datetime     null comment '创建时间',
    LAST_UPDATE_USER varchar(50)  null comment '更新人',
    LAST_UPDATE_TIME datetime     null comment '最后更新时间'
)
    comment '用户表' charset = utf8;

-- 角色表
create table t_role
(
    ID               bigint auto_increment comment 'ID'
        primary key,
    ROLE_NAME        varchar(100) not null comment '角色名称',
    REMARK           varchar(100) null comment '角色描述',
    CREATE_USER      varchar(50)  null comment '创建用户',
    CREATE_TIME      datetime     null comment '创建时间',
    LAST_UPDATE_USER varchar(50)  null comment '更新人',
    LAST_UPDATE_TIME datetime     null comment '最后更新时间'
)
    comment '角色表' charset = utf8;

-- 菜单表
create table t_menu
(
    ID               bigint auto_increment comment '菜单/按钮ID'
        primary key,
    PARENT_ID        bigint       not null comment '上级菜单ID',
    MENU_NAME        varchar(50)  not null comment '菜单/按钮名称',
    URL              varchar(100) null comment '菜单URL',
    TIER             int          null comment '层级,1 一级 2 二级 3 三级',
    PERMS            varchar(20)  null comment '权限标识',
    ICON             varchar(30)  null comment '图标',
    TYPE             varchar(2)   not null comment '类型 0菜单 1按钮',
    ORDER_NUM        int          null comment '排序',
    STATUS           int comment '状态，0无效，1有效',
    CREATE_USER      varchar(50)  null comment '创建用户',
    CREATE_TIME      datetime     null comment '创建时间',
    LAST_UPDATE_USER varchar(50)  null comment '更新人',
    LAST_UPDATE_TIME datetime     null comment '最后更新时间'
)
    comment '菜单表' charset = utf8;

INSERT INTO examples.t_menu (ID, PARENT_ID, MENU_NAME, URL, TIER, PERMS, ICON, TYPE, ORDER_NUM, STATUS, CREATE_USER,
                             CREATE_TIME, LAST_UPDATE_USER, LAST_UPDATE_TIME)
VALUES (1, 0, '系统管理', null, 1, null, null, '1', 1, 1, null, null, null, null);
INSERT INTO examples.t_menu (ID, PARENT_ID, MENU_NAME, URL, TIER, PERMS, ICON, TYPE, ORDER_NUM, STATUS, CREATE_USER,
                             CREATE_TIME, LAST_UPDATE_USER, LAST_UPDATE_TIME)
VALUES (2, 1, '首页', '/sys/home', 2, null, null, '1', 1, 1, null, null, null, null);
INSERT INTO examples.t_menu (ID, PARENT_ID, MENU_NAME, URL, TIER, PERMS, ICON, TYPE, ORDER_NUM, STATUS, CREATE_USER,
                             CREATE_TIME, LAST_UPDATE_USER, LAST_UPDATE_TIME)
VALUES (3, 1, '用户管理', '/sys/user/index', 2, null, null, '1', 1, 1, null, null, null, null);
INSERT INTO examples.t_menu (ID, PARENT_ID, MENU_NAME, URL, TIER, PERMS, ICON, TYPE, ORDER_NUM, STATUS, CREATE_USER,
                             CREATE_TIME, LAST_UPDATE_USER, LAST_UPDATE_TIME)
VALUES (4, 1, '角色管理', '/sys/role/index', 2, null, null, '1', 1, 1, null, null, null, null);
INSERT INTO examples.t_menu (ID, PARENT_ID, MENU_NAME, URL, TIER, PERMS, ICON, TYPE, ORDER_NUM, STATUS, CREATE_USER,
                             CREATE_TIME, LAST_UPDATE_USER, LAST_UPDATE_TIME)
VALUES (5, 1, '菜单管理', '/sys/menu/index', 2, null, null, '1', 1, 1, null, null, null, null);
INSERT INTO examples.t_menu (ID, PARENT_ID, MENU_NAME, URL, TIER, PERMS, ICON, TYPE, ORDER_NUM, STATUS, CREATE_USER,
                             CREATE_TIME, LAST_UPDATE_USER, LAST_UPDATE_TIME)
VALUES (6, 1, 'ceshi', '2', null, null, '1', '0', 2, null, null, '2019-11-06 04:15:17', null, null);


-- 用户角色表
create table t_user_role
(
    ID               bigint auto_increment
        primary key,
    USER_ID          bigint      not null comment '用户表ID',
    ROLE_ID          bigint      not null comment '角色表ID',
    CREATE_USER      varchar(50) null comment '创建用户',
    CREATE_TIME      datetime    null comment '创建时间',
    LAST_UPDATE_USER varchar(50) null comment '更新人',
    LAST_UPDATE_TIME datetime    null comment '最后更新时间'
)
    comment '用户角色表' charset = utf8;


-- 角色菜单表
create table t_role_menu
(
    ID               bigint auto_increment
        primary key,
    ROLE_ID          bigint      not null comment '角色ID',
    MENU_ID          bigint      not null comment '菜单/按钮ID',
    CREATE_USER      varchar(50) null comment '创建用户',
    CREATE_TIME      datetime    null comment '创建时间',
    LAST_UPDATE_USER varchar(50) null comment '更新人',
    LAST_UPDATE_TIME datetime    null comment '最后更新时间'
)
    comment '角色菜单表';


-- 配置
create table t_generator_config
(
    ID                   int                            not null comment '主键'
        primary key,
    AUTHOR               varchar(20)                    not null comment '作者',
    BASE_PACKAGE         varchar(50)                    not null comment '基础包名',
    ENTITY_PACKAGE       varchar(20)                    not null comment 'ENTITY文件存放路径',
    MAPPER_PACKAGE       varchar(20)                    not null comment 'MAPPER文件存放路径',
    MAPPER_XML_PACKAGE   varchar(20)                    not null comment 'MAPPER XML文件存放路径',
    SERVICE_PACKAGE      varchar(20)                    not null comment 'SERVICE文件存放路径',
    SERVICE_IMPL_PACKAGE varchar(20)                    not null comment 'SERVICE IMPL文件存放路径',
    CONTROLLER_PACKAGE   varchar(20)                    not null comment 'CONTROLLER文件存放路径',
    IS_TRIM              varchar(2)                     not null comment '是否去除前缀 1是 0否',
    TRIM_VALUE           varchar(10)                    null comment '前缀内容',
    GEN_PATH             varchar(50) default 'gen_tmp/' null comment '生成路径',
    CREATE_USER          varchar(50)                    null comment '创建用户',
    CREATE_TIME          datetime                       null comment '创建时间',
    LAST_UPDATE_USER     varchar(50)                    null comment '更新人',
    LAST_UPDATE_TIME     datetime                       null comment '最后更新时间'
)
    comment '代码生成配置表' charset = utf8;

-- mcc 码
create table t_mcc
(
    ID               bigint auto_increment
        primary key,
    MCC              varchar(4)   null comment 'MCC码类别',
    MCC_NAME         varchar(100) null comment 'MCC类别名称',
    MCC_BIG_CAT      varchar(1)   null comment 'MCC所属大类',
    MCC_SMALL_CAT    varchar(2)   null comment 'MCC小类',
    MCC_DETAIL_CAT   varchar(20)  null comment 'MCC细类',
    CREATE_USER      varchar(50)  null comment '创建用户',
    CREATE_TIME      datetime     null comment '创建时间',
    LAST_UPDATE_USER varchar(50)  null comment '更新人',
    LAST_UPDATE_TIME datetime     null comment '最后更新时间'
)
    comment 'MCC表';


-- 银行代码
drop table if exists t_bank;
create table t_bank
(
    ID               bigint auto_increment comment '主键ID'
        primary key,
    BANK_CODE        varchar(12)  not null comment '银行代码',
    BANK_NAME        varchar(50)  not null comment '银行名称',
    SHORT_NAME       varchar(20)  not null comment '银行简称',
    EN_NAME          varchar(20)  null comment '英文缩写',
    BANK_LOGO        varchar(200) null comment '银行logo地址',
    ORDER_NUM        int default 999 null comment '排序',
    IPAY_BANK_CODE   varchar(12)  null comment '通联ipay代码',
    KFT_BANK_CODE    varchar(12)  null comment '快付通代码',
    IS_ENABLE        int          null comment '是否启用 0:禁用1启用',
    CREATE_USER      varchar(50)  null comment '创建用户',
    CREATE_TIME      datetime     null comment '创建时间',
    LAST_UPDATE_USER varchar(50)  null comment '更新人',
    LAST_UPDATE_TIME datetime     null comment '最后更新时间'
)
    comment '银行管理表', charset = utf8mb4;

ALTER TABLE t_bank ADD INDEX IDX_t_bank(BANK_CODE);


-- t_card_bin_info
create table t_card_bin_info
(
    ID               bigint auto_increment
        primary key,
    CARD_BIN         varchar(20)   not null comment '主键:卡BIN号',
    CARD_BIN_LEN     int           null comment '卡BIN长度',
    CARD_NO_LEN      int           not null comment '卡号长度',
    CARD_TYPE        varchar(4)    null comment '卡类型',
    CARD_TEMPLATE    varchar(32)   null comment '卡号模板',
    BANK_CODE        varchar(21)   null comment '人行行号',
    BANK_NAME        varchar(128)  null comment '银行名称',
    BANK_SHORT_NAME  varchar(64)   null comment '银行简称',
    STATUS           int default 1 null comment '0无效，1有效',
    CREATE_USER      varchar(50)   null comment '创建用户',
    CREATE_TIME      datetime      null comment '创建时间',
    LAST_UPDATE_USER varchar(50)   null comment '更新人',
    LAST_UPDATE_TIME datetime      null comment '最后更新时间'
)
    comment '卡bin表' charset = utf8mb4;


-- t_region 地区码
DROP TABLE IF EXISTS t_region;
create table t_region
(
    ID               bigint auto_increment
        primary key,
    REGION_CODE      varchar(6)   not null comment '区域主键',
    REGION_P_CODE    varchar(6)   null comment '区域上级标识',
    REGION_NAME      varchar(50)  null comment '区域名称',
    EN_NAME          varchar(50)  null comment '英文名称',
    SHORT_NAME       varchar(50)  null comment '地名简称',
    PINYIN           varchar(100) null comment '拼音',
    LEVEL            int          null comment '区域等级，0中国，1省级，2市级，3县级，',
    COMBINATION_NAME varchar(100) null comment '组合名称',
    ZIP_CODE         varchar(20)  null comment '邮政编码',
    LONGITUDE        varchar(10)  null comment '经度',
    LATITUDE         varchar(10)  null comment '纬度',
    CREATE_USER      varchar(50)  null comment '创建用户',
    CREATE_TIME      datetime     null comment '创建时间',
    LAST_UPDATE_USER varchar(50)  null comment '更新人',
    LAST_UPDATE_TIME datetime     null comment '最后更新时间'
)
    comment '地区码表', charset = utf8mb4;







