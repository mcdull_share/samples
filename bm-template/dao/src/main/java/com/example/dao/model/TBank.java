package com.example.dao.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.dao.common.model.BaseModel;
import lombok.*;

import java.io.Serializable;
import java.util.Date;


/**
 * 银行管理表 Entity
 *
 * @author majian
 * @date 2019-11-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("t_bank")
public class TBank extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;

    /**
     * 银行代码
     */
    @TableField("BANK_CODE")
    private String bankCode;

    /**
     * 银行名称
     */
    @TableField("BANK_NAME")
    private String bankName;

    /**
     * 银行简称
     */
    @TableField("SHORT_NAME")
    private String shortName;

    /**
     * 英文缩写
     */
    @TableField("EN_NAME")
    private String enName;

    /**
     * 银行logo地址
     */
    @TableField("BANK_LOGO")
    private String bankLogo;

    /**
     * 排序
     */
    @TableField("ORDER_NUM")
    private Integer orderNum;

    /**
     * 通联ipay代码
     */
    @TableField("IPAY_BANK_CODE")
    private String ipayBankCode;

    /**
     * 快付通代码
     */
    @TableField("KFT_BANK_CODE")
    private String kftBankCode;

    /**
     * 是否启用 0:禁用1启用
     */
    @TableField("IS_ENABLE")
    private Integer isEnable;

    /**
     * 创建用户
     */
    @TableField("CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 更新人
     */
    @TableField("LAST_UPDATE_USER")
    private String lastUpdateUser;

    /**
     * 最后更新时间
     */
    @TableField("LAST_UPDATE_TIME")
    private Date lastUpdateTime;

}
