package com.example.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dao.model.TGeneratorConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author bryceM
 */
public interface GeneratorConfigMapper extends BaseMapper<TGeneratorConfig> {

    List<String> getDatabases();

    List<Map<String, Object>> getTables(@Param("schemaName") String schemaName, @Param("tableName") String tableName);

    List<Map<String, Object>> getColumns(@Param("schemaName") String schemaName, @Param("tableName") String tableName);


}
