package com.example.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dao.model.TRegion;

import java.util.List;
import java.util.Map;

/**
 *  Mapper
 *
 * @author majian
 * @date 2019-11-11
 */
public interface TRegionMapper extends BaseMapper<TRegion> {

    /**
    * 自定义分页
    * @param params
    * @return
    */
    List<TRegion> findList(Map<String, String> params);


}
