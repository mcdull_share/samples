package com.example.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dao.model.TUser;

import java.util.List;
import java.util.Map;

/**
* t_user
* 
* 2019-11-04
*/
public interface TUserMapper extends BaseMapper<TUser> {

    /**
     * 自定义分页
     * @param params
     * @return
     */
    List<TUser> findList(Map<String, Object> params);
}