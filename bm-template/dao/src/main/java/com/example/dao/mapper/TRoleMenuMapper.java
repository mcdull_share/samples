package com.example.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dao.model.TMenu;
import com.example.dao.model.TRoleMenu;

/**
* t_role_menu
* 
* 2019-11-04
*/
public interface TRoleMenuMapper extends BaseMapper<TRoleMenu> {

}