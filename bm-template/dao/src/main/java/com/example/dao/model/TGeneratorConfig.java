package com.example.dao.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.dao.common.model.BaseModel;
import lombok.*;

import java.io.Serializable;

/**
 * @author bryceM
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("t_generator_config")
public class TGeneratorConfig extends BaseModel implements Serializable {

    public static final String TRIM_YES = "1";
    public static final String TRIM_NO = "0";

    /**
     * ID
     */
    @TableId(value = "ID", type = IdType.AUTO)
    public Long id;

    /**
     * 作者
     */
    @TableField("AUTHOR")
    private String author;

    /**
     * 基础包名
     */
    @TableField("BASE_PACKAGE")
    private String basePackage;

    /**
     * entity文件存放路径
     */
    @TableField("ENTITY_PACKAGE")
    private String entityPackage;

    /**
     * mapper文件存放路径
     */
    @TableField("MAPPER_PACKAGE")
    private String mapperPackage;

    /**
     * mapper xml文件存放路径
     */
    @TableField("MAPPER_XML_PACKAGE")
    private String mapperXmlPackage;

    /**
     * servcie文件存放路径
     */
    @TableField("SERVICE_PACKAGE")
    private String servicePackage;

    /**
     * serviceImpl文件存放路径
     */
    @TableField("SERVICE_IMPL_PACKAGE")
    private String serviceImplPackage;

    /**
     * controller文件存放路径
     */
    @TableField("CONTROLLER_PACKAGE")
    private String controllerPackage;

    /**
     * 是否去除前缀
     */
    @TableField("IS_TRIM")
    private String isTrim;

    /**
     * 前缀内容
     */
    @TableField("TRIM_VALUE")
    private String trimValue;


    /**
     * 生成路径
     */
    @TableField("GEN_PATH")
    private String genPath;

    /**
     * java文件路径，固定值
     */
    @Builder.Default
    private transient String javaPath = "/src/main/java/";
    /**
     * 配置文件存放路径，固定值
     */
    @Builder.Default
    private transient String resourcesPath = "src/main/resources";
    /**
     * 文件生成日期
     */
    private transient String date;

    /**
     * 表名
     */
    private transient String tableName;
    /**
     * 表注释
     */
    private transient String tableComment;
    /**
     * 数据表对应的类名
     */
    private transient String className;

}