package com.example.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dao.model.TUserRole;

/**
* t_user_role
* 
* 2019-11-04
*/
public interface TUserRoleMapper extends BaseMapper<TUserRole> {

}