package com.example.dao.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.dao.common.model.BaseModel;
import lombok.*;

import java.io.Serializable;
import java.util.Date;


/**
 *  Entity
 *
 * @author majian
 * @date 2019-11-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("t_region")
public class TRegion extends BaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;

    /**
     * 区域主键
     */
    @TableField("REGION_CODE")
    private String regionCode;

    /**
     * 区域上级标识
     */
    @TableField("REGION_P_CODE")
    private String regionPCode;

    /**
     * 区域名称
     */
    @TableField("REGION_NAME")
    private String regionName;

    /**
     * 英文名称
     */
    @TableField("EN_NAME")
    private String enName;

    /**
     * 地名简称
     */
    @TableField("SHORT_NAME")
    private String shortName;

    /**
     * 拼音
     */
    @TableField("PINYIN")
    private String pinyin;

    /**
     * 区域等级
     */
    @TableField("LEVEL")
    private Integer level;

    /**
     * 组合名称
     */
    @TableField("COMBINATION_NAME")
    private String combinationName;

    /**
     * 邮政编码
     */
    @TableField("ZIP_CODE")
    private String zipCode;

    /**
     * 经度
     */
    @TableField("LONGITUDE")
    private String longitude;

    /**
     * 纬度
     */
    @TableField("LATITUDE")
    private String latitude;

    /**
     * 创建用户
     */
    @TableField("CREATE_USER")
    private String createUser;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 更新人
     */
    @TableField("LAST_UPDATE_USER")
    private String lastUpdateUser;

    /**
     * 最后更新时间
     */
    @TableField("LAST_UPDATE_TIME")
    private Date lastUpdateTime;

}
