package com.example.dao.common.plugin;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.internal.types.JavaTypeResolverDefaultImpl;

/**
 * @author jonas
 * @date 2019-03-13.
 */
public class MyJavaTypeResolver extends JavaTypeResolverDefaultImpl {
    public MyJavaTypeResolver(){
        super();
    }

    @Override
    protected FullyQualifiedJavaType calculateBigDecimalReplacement(final IntrospectedColumn column,
                                                                    final FullyQualifiedJavaType defaultType) {
        if (column.getScale() > 0 || forceBigDecimals) {
            return defaultType;
        }
        if (column.getLength() > 9) {
            return new FullyQualifiedJavaType(Long.class.getName());
        }
        if (column.getLength() > 4) {
            return new FullyQualifiedJavaType(Integer.class.getName());
        }
        return new FullyQualifiedJavaType(Short.class.getName());
    }
}
