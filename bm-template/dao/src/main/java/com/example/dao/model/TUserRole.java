package com.example.dao.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.dao.common.model.BaseModel;
import lombok.*;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("t_user_role")
public class TUserRole extends BaseModel implements Serializable {

    /**
     * ID
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;

    /**
     * 用户表ID
     */
    @TableField("USER_ID")
    private Long userId;

    /**
     * 角色表ID
     */
    @TableField("ROLE_ID")
    private Long roleId;


    private static final long serialVersionUID = 1L;


}