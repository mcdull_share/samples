package com.example.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dao.model.TRole;

import java.util.List;
import java.util.Map;

/**
 * t_role
 * <p>
 * 2019-11-04
 */
public interface TRoleMapper extends BaseMapper<TRole> {

    /**
     * 自定义分页
     * @param params
     * @return
     */
    List<TRole> findList(Map<String, Object> params);

}