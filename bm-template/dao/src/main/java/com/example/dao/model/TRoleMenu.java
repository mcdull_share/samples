package com.example.dao.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.dao.common.model.BaseModel;
import lombok.*;

import java.io.Serializable;


@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("t_role_menu")
public class TRoleMenu  extends BaseModel implements Serializable {

    /**
     * ID
     */
    @TableId(value = "ID", type = IdType.AUTO)
    public Long id;

    /**
     * 角色ID
     */
     @TableField("ROLE_ID")
    private Long roleId;

    /**
     * 菜单/按钮ID
     */
     @TableField("MENU_ID")
    private Long menuId;

    private static final long serialVersionUID = 1L;

}