package com.example.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dao.model.TMenu;

import java.util.List;
import java.util.Map;

/**
* t_menu
* 
* 2019-11-04
*/
public interface TMenuMapper extends BaseMapper<TMenu> {

    /**
     * 自定义分页
     * @param params
     * @return
     */
    List<TMenu> findList(Map<String, Object> params);

}