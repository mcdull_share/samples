package com.example.dao.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.dao.common.model.BaseModel;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户信息表
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("t_user")
public class TUser extends BaseModel implements Serializable {

    /**
     * ID
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;

    /**
     * 登陆名
     */
    @TableField("LOGIN_NAME")
    private String loginName;

    /**
     * 用户名
     */
    @TableField("USER_NAME")
    private String userName;

    /**
     * 密码
     */
    @TableField("PASSWORD")
    private String password;

    /**
     * 邮箱
     */
    @TableField("EMAIL")
    private String email;

    /**
     * 联系电话
     */
    @TableField("PHONE")
    private String phone;

    /**
     * 状态 0锁定 1有效
     */
    @TableField("STATUS")
    private Integer status;

    /**
     * 性别 0男 1女 2保密
     */
    @TableField("SEX")
    private Integer sex;

    /**
     * 头像
     */
    @TableField("HEAD_ICON")
    private String headIcon;

    /**
     * 用户类型：1：总部管理员 2：分公司管理员 3：代理商管理员
     */
    @TableField("USER_TYPE")
    private Integer userType;

    /**
     * 描述
     */
    @TableField("DESCRIPTION")
    private String description;

    /**
     * 部门ID
     */
    @TableField("DEPT_ID")
    private String deptId;

    /**
     * 最近访问时间
     */
    @TableField(value = "LAST_LOGIN_TIME")
    private Date lastLoginTime;

    private static final long serialVersionUID = 1L;

}