package com.example.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.dao.model.TBank;

import java.util.List;
import java.util.Map;

/**
 * 银行管理表 Mapper
 *
 * @author majian
 * @date 2019-11-11
 */
public interface TBankMapper extends BaseMapper<TBank> {

    /**
    * 自定义分页
    * @param params
    * @return
    */
    List<TBank> findList(Map<String, String> params);


}
