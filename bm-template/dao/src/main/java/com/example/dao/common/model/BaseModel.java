package com.example.dao.common.model;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class BaseModel implements Serializable {

    /**
     * 创建用户
     */
    @TableField(value = "CREATE_USER", fill = FieldFill.INSERT)
    public String createUser;

    /**
     * 创建时间
     */
    @TableField(value = "CREATE_TIME", fill = FieldFill.INSERT)
    public Date createTime;

    /**
     * 更新人
     */
    @TableField(value = "LAST_UPDATE_USER", fill = FieldFill.UPDATE)
    public String lastUpdateUser;

    /**
     * 最后更新时间
     */
    @TableField(value = "LAST_UPDATE_TIME", fill = FieldFill.UPDATE)
    public Date lastUpdateTime;

    private static final long serialVersionUID = 1L;
}
