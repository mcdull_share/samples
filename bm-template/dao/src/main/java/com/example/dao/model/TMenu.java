package com.example.dao.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.dao.common.model.BaseModel;
import lombok.*;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("t_menu")
public class TMenu  extends BaseModel implements Serializable {

    /**
     * ID
     */
    @TableId(value = "ID", type = IdType.AUTO)
    public Long id;

    /**
     * 上级菜单ID
     */
    @TableField("PARENT_ID")
    private Long parentId;

    /**
     * 菜单/按钮名称
     */
     @TableField("MENU_NAME")
    private String menuName;

    /**
     * 菜单URL
     */
     @TableField("URL")
    private String url;

    /**
     * 层级,1 一级 2 二级 3 三级
     */
     @TableField("TIER")
    private Integer tier;

    /**
     * 权限标识
     */
     @TableField("PERMS")
    private String perms;

    /**
     * 图标
     */
     @TableField("ICON")
    private String icon;

    /**
     * 类型 0菜单 1按钮
     */
     @TableField("TYPE")
    private String type;

    /**
     * 排序
     */
     @TableField("ORDER_NUM")
    private Integer orderNum;

    /**
     * 状态，0无效，1有效
     */
     @TableField("STATUS")
    private Integer status;

    private static final long serialVersionUID = 1L;

}