package com.example.bm;

import com.alibaba.excel.EasyExcel;
import com.example.bm.excel.BankDataListener;
import com.example.bm.excel.ExcelBankEntity;
import com.example.bm.system.service.BankService;
import com.example.bm.system.service.RegionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BmApplication.class)
public class BmApplicationTests {

    @Autowired
    private RegionService regionService;
    @Autowired
    private BankService bankService;


    @Test
    public void loadBankData() throws FileNotFoundException {
        // 有个很重要的点 DemoDataListener 不能被spring管理，要每次读取excel都要new,然后里面用到spring可以构造方法传进去
        // 写法1：
        File file = ResourceUtils.getFile("classpath:bank.xlsx");
        InputStream inputStream = new FileInputStream(file);
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        EasyExcel.read(file, ExcelBankEntity.class, new BankDataListener(regionService, bankService)).sheet().doRead();

    }

}
