package com.example.bm.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;


@Data
public class ExcelBankEntity {

    //@ExcelProperty(index = 2)
    //private String cityCode;

    @ExcelProperty(index = 3)
    private String fullName;

    @ExcelProperty(index = 4)
    private String bankName;

    @ExcelProperty(index = 5)
    private String bankCode;

}
