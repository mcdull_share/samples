package com.example.bm.system.controller;

import cn.hutool.core.convert.Convert;
import com.example.bm.system.service.RoleService;
import com.example.bm.system.service.UserService;
import com.example.common.domain.Layui;
import com.example.dao.model.TRole;
import com.example.dao.model.TUser;
import com.github.pagehelper.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 用户controller
 */
@Slf4j
@Controller
@RequestMapping("/sys/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    /**
     * 用户列表
     *
     * @param modelAndView
     * @return
     */
    @GetMapping(value = "/index")
    public ModelAndView index(ModelAndView modelAndView) {
        modelAndView.setViewName("views/system/user/userList");
        return modelAndView;
    }

    /**
     * 明细
     *
     * @param params
     * @return
     */
    @RequestMapping("/list")
    @ResponseBody
    public Layui<List<TUser>> list(@RequestParam Map<String, Object> params, HttpServletRequest request) {
        log.info("用户明细 queryList={}", params);
        int pageNum = Convert.toInt(params.get("page"));
        int pageSize = Convert.toInt(params.get("limit"));
        Page<TUser> list = userService.getPageList(params, pageNum, pageSize);
        return Layui.success(list.getResult(), list.getTotal());
    }

    /**
     * 明细
     *
     * @param params
     * @return
     */
    @RequestMapping("/listAll")
    @ResponseBody
    public Layui<List<TUser>> listAll(@RequestParam Map<String, Object> params, HttpServletRequest request) {
        log.info("查询所以 queryList={}", params);
        List<TUser> list = userService.getBaseMapper().selectList(null);
        return Layui.success(list, (long) list.size());
    }

    /**
     * 添加
     *
     * @param modelAndView
     * @return
     */
    @GetMapping(value = "/addPage")
    public ModelAndView addPage(ModelAndView modelAndView) {
        modelAndView.setViewName("views/system/user/userAdd");
        List<TRole> roles = roleService.getList(null);
        //角色列表
        modelAndView.addObject("roles", roles);
        return modelAndView;
    }

    /**
     * 添加
     *
     * @param params
     * @param request
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public Layui<String> add(@RequestParam Map<String, String> params, @ModelAttribute TUser user, HttpServletRequest request) {
        log.info("用户添加 params={}", params);
        userService.createInfo(user, params);
        return Layui.success();
    }


    /**
     * 编辑
     *
     * @param modelAndView
     * @return
     */
    @GetMapping(value = "/editPage")
    public ModelAndView editPage(ModelAndView modelAndView, @RequestParam Map<String, String> params) {
        modelAndView.setViewName("views/system/user/userEdit");
        TUser user = userService.getById(params.get("id"));
        modelAndView.addObject("info", user);
        List<TRole> roles = roleService.getList(null);
        //角色列表
        modelAndView.addObject("roles", roles);
        return modelAndView;
    }

    /**
     * 添加
     *
     * @param params
     * @param request
     * @return
     */
    @RequestMapping("/edit")
    @ResponseBody
    public Layui<String> edit(@RequestParam Map<String, String> params, @ModelAttribute TUser user, HttpServletRequest request) {
        log.info("用户编辑 params={}", params);
        userService.updateInfo(user, params);
        return Layui.success();
    }

}
