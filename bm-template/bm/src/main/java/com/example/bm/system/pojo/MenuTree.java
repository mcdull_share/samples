package com.example.bm.system.pojo;

import com.example.dao.model.TMenu;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * layui menu tree
 */
@Data
@NoArgsConstructor
public class MenuTree<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    //节点标题
    private String title;
    //
    private String id;
    //节点字段名,一般对应表字段名
    private String field;
    private String href;
    //子节点
    private List<MenuTree<T>> children = new ArrayList<>();

    private boolean spread = true;
    private boolean checked = false;
    private boolean disabled = false;

    private String parentId;
    private boolean hasParent = false;
    private boolean hasChild = false;
    private String icon;


    private TMenu data;

}