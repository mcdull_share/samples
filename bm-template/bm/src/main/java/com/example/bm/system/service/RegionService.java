package com.example.bm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.dao.model.TRegion;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 *  Service接口
 *
 * @author majian
 * @date 2019-11-11
 */
public interface RegionService extends IService<TRegion> {
    /**
     * 查询（自定义分页）
     *
     * @param params
     * @return Page<TRegion>
     */
    Page<TRegion> getPageList(Map<String, String> params, int pageNum, int pageSize);

    /**
    * 不分页
    *
    * @param params
    * @return
    */
    List<TRegion> getList(Map<String, String> params);

    /**
     * 查询（所有）
     *
     * @param tRegion tRegion
     * @return List<TRegion>
     */
    List<TRegion> findTRegions(TRegion tRegion);

    /**
     * 新增
     *
     * @param tRegion tRegion
     */
    void createTRegion(TRegion tRegion);

    /**
     * 修改
     *
     * @param tRegion tRegion
     */
    void updateTRegion(TRegion tRegion);

    /**
     * 删除
     *
     * @param tRegion tRegion
     */
    void deleteTRegion(TRegion tRegion);
}
