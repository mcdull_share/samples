package com.example.bm.system.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户信息表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserModel implements Serializable {
    /**
     * ID
     */
    private Long id;

    /**
     * 登陆名
     */
    private String loginName;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 状态 0锁定 1有效
     */
    private Integer status;

    /**
     * 性别 0男 1女 2保密
     */
    private Integer sex;

    /**
     * 头像
     */
    private String headIcon;

    /**
     * 用户类型：1：总部管理员 2：分公司管理员 3：代理商管理员
     */
    private Integer userType;

    private String userTypeDesc;

    /**
     * 描述
     */
    private String description;

    private Long roleId;
    private String roleName;


    private String oldPassword;

    private String newPassword;

    private String confirmPassword;


    /**
     * 部门ID
     */
    private String deptId;

    /**
     * 最近访问时间
     */
    private Date lastLoginTime;
    /**
     * 创建用户
     */
    private String createUser;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新人
     */
    private String lastUpdateUser;

    /**
     * 最后更新时间
     */
    private Date lastUpdateTime;

    private static final long serialVersionUID = 1L;

}