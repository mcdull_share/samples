package com.example.bm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.dao.model.TUser;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * UserService Service接口
 */
public interface UserService extends IService<TUser> {

    /**
     * 查询列表, 自定义分页
     *
     * @param params
     * @return
     */
    Page<TUser> getPageList(Map<String, Object> params, int pageNum, int pageSize);

    /**
     * 不分页
     *
     * @param params
     * @return
     */
    List<TUser> getList(Map<String, Object> params);


    /**
     * 新增
     *
     * @param user
     */
    void createInfo(TUser user, Map<String, String> params);

    /**
     * 更新
     *
     * @param user
     */
    void updateInfo(TUser user, Map<String, String> params);


    void deleteInfo(TUser user);
}
