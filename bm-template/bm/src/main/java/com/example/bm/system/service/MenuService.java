package com.example.bm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.bm.system.pojo.MenuFormSelect;
import com.example.bm.system.pojo.MenuTree;
import com.example.dao.model.TMenu;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * UserService Service接口
 */
public interface MenuService extends IService<TMenu> {

    /**
     * s
     * 查询列表, 自定义分页
     *
     * @param params
     * @return
     */
    Page<TMenu> getPageList(Map<String, Object> params, int pageNum, int pageSize);

    /**
     * 不分页
     *
     * @param params
     * @return
     */
    List<MenuTree<TMenu>> getMenuTree(Map<String, Object> params);


    List<MenuTree<TMenu>> getRoleMenuList(Map<String, Object> params);


    /**
     *
     * @param params
     * @return
     */
    List<MenuFormSelect<TMenu>> getFormSelectMenu(Map<String, Object> params);


    /**
     * 新增
     *
     * @param info
     */
    void createInfo(TMenu info);

    /**
     * 更新
     *
     * @param info
     */
    void updateInfo(TMenu info);


    void deleteInfo(TMenu info);

}
