package com.example.bm.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.bm.system.service.BankService;
import com.example.dao.mapper.TBankMapper;
import com.example.dao.model.TBank;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 * 银行管理表 Service实现
 *
 * @author majian
 * @date 2019-11-11
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class BankServiceImpl extends ServiceImpl<TBankMapper, TBank> implements BankService {


    /**
    * 查询列表, 自定义分页
    *
    * @param params
    * @return
    */
    @Override
    public Page<TBank> getPageList(Map<String, String> params, int pageNum, int pageSize) {
        return PageHelper.startPage(pageNum, pageSize).doSelectPage(() -> this.baseMapper.findList(params));
    }

    /**
    * 不分页
    *
    * @param params
    * @return
    */
    @Override
    public List<TBank> getList(Map<String, String> params) {
        return this.baseMapper.findList(params);
    }


    @Override
    public List<TBank> findTBanks(TBank tBank) {
	    LambdaQueryWrapper<TBank> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional
    public void createTBank(TBank tBank) {
        this.save(tBank);
    }

    @Override
    @Transactional
    public void updateTBank(TBank tBank) {
        this.saveOrUpdate(tBank);
    }

    @Override
    @Transactional
    public void deleteTBank(TBank tBank) {
        LambdaQueryWrapper<TBank> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}
}
