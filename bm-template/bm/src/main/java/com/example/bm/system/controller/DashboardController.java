package com.example.bm.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class DashboardController {

    /**
     * 首页
     *
     * @param modelAndView
     * @return
     */
    @GetMapping(value = "/index")
    public ModelAndView index(ModelAndView modelAndView) {
        modelAndView.setViewName("views/index");
        return modelAndView;
    }

    /**
     * Dashboard
     *
     * @param modelAndView
     * @return
     */
    @GetMapping(value = "/console")
    public ModelAndView console(ModelAndView modelAndView) {
        modelAndView.setViewName("views/home/console");
        return modelAndView;
    }


}
