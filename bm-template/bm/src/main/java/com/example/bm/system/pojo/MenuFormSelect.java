package com.example.bm.system.pojo;

import com.example.dao.model.TMenu;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * MenuFormSelect menu tree
 */
@Data
@NoArgsConstructor
public class MenuFormSelect<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private String id;
    private boolean open = false;
    //子节点
    private List<MenuFormSelect<T>> children = new ArrayList<>();

    private String parentId;
    private boolean hasParent = false;
    private boolean hasChild = false;

    //菜单信息
    private TMenu data;

}