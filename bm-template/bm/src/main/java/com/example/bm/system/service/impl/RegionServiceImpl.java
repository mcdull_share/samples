package com.example.bm.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.bm.system.service.RegionService;
import com.example.dao.mapper.TRegionMapper;
import com.example.dao.model.TRegion;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 *  Service实现
 *
 * @author majian
 * @date 2019-11-11
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class RegionServiceImpl extends ServiceImpl<TRegionMapper, TRegion> implements RegionService {


    /**
    * 查询列表, 自定义分页
    *
    * @param params
    * @return
    */
    @Override
    public Page<TRegion> getPageList(Map<String, String> params, int pageNum, int pageSize) {
        return PageHelper.startPage(pageNum, pageSize).doSelectPage(() -> this.baseMapper.findList(params));
    }

    /**
    * 不分页
    *
    * @param params
    * @return
    */
    @Override
    public List<TRegion> getList(Map<String, String> params) {
        return this.baseMapper.findList(params);
    }


    @Override
    public List<TRegion> findTRegions(TRegion tRegion) {
	    LambdaQueryWrapper<TRegion> queryWrapper = new LambdaQueryWrapper<>();
		// TODO 设置查询条件
		return this.baseMapper.selectList(queryWrapper);
    }

    @Override
    @Transactional
    public void createTRegion(TRegion tRegion) {
        this.save(tRegion);
    }

    @Override
    @Transactional
    public void updateTRegion(TRegion tRegion) {
        this.saveOrUpdate(tRegion);
    }

    @Override
    @Transactional
    public void deleteTRegion(TRegion tRegion) {
        LambdaQueryWrapper<TRegion> wrapper = new LambdaQueryWrapper<>();
	    // TODO 设置删除条件
	    this.remove(wrapper);
	}
}
