package com.example.bm.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.bm.common.util.MenuTreeUtil;
import com.example.bm.system.pojo.MenuFormSelect;
import com.example.bm.system.pojo.MenuTree;
import com.example.bm.system.service.MenuService;
import com.example.dao.mapper.TMenuMapper;
import com.example.dao.mapper.TRoleMenuMapper;
import com.example.dao.model.TMenu;
import com.example.dao.model.TRoleMenu;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 用户service
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class MenuServiceImpl extends ServiceImpl<TMenuMapper, TMenu> implements MenuService {

    @Resource
    private TRoleMenuMapper roleMenuService;

    /**
     * 查询列表, 自定义分页
     *
     * @param params
     * @return
     */
    @Override
    public Page<TMenu> getPageList(Map<String, Object> params, int pageNum, int pageSize) {
        return PageHelper.startPage(pageNum, pageSize).doSelectPage(() -> this.baseMapper.findList(params));
    }

    /**
     * 不分页
     *
     * @param params
     * @return
     */
    @Override
    public List<MenuTree<TMenu>> getMenuTree(Map<String, Object> params) {
        List<TMenu> list = this.baseMapper.findList(params);
        List<MenuTree<TMenu>> menuTrees = convertMenus(list);
        return MenuTreeUtil.buildMenuTree(menuTrees);
    }

    /**
     * 不分页,查询拥有权限，并选中
     *
     * @param params
     * @return
     */
    @Override
    public List<MenuTree<TMenu>> getRoleMenuList(Map<String, Object> params) {
        List<TMenu> list = this.baseMapper.findList(null);
        //1.查询该角色拥有的权限
        LambdaQueryWrapper<TRoleMenu> wrapper = new LambdaQueryWrapper<TRoleMenu>()
                .eq(TRoleMenu::getRoleId, params.get("id"));
        List<TRoleMenu> roleMenuList = roleMenuService.selectList(wrapper);
        List<MenuTree<TMenu>> menuTrees = convertMenus(list);
        //遍历选中
        roleMenuList.forEach(obj -> {
            menuTrees.forEach(tmp -> {
                if (obj.getMenuId().equals(Long.parseLong(tmp.getId()))) {
                    tmp.setChecked(true);
                }
            });
        });
        return MenuTreeUtil.buildMenuTree(menuTrees);
    }


    /**
     * 不分页
     *
     * @param params
     * @return
     */
    @Override
    public List<MenuFormSelect<TMenu>> getFormSelectMenu(Map<String, Object> params) {
        List<TMenu> list = this.baseMapper.findList(params);
        List<MenuFormSelect<TMenu>> menuTrees = convertFormSelectMenu(list);
        return MenuTreeUtil.buildFormSelectMenuTree(menuTrees);
    }


    /**
     * 新增
     *
     * @param info
     */
    @Override
    @Transactional
    public void createInfo(TMenu info) {
        this.save(info);
    }

    /**
     * 更新
     *
     * @param info
     */
    @Override
    @Transactional
    public void updateInfo(TMenu info) {
        LambdaQueryWrapper<TMenu> wrapper = new LambdaQueryWrapper<>();
        //TODO 更新条件
        this.saveOrUpdate(info);
    }


    /**
     * 删除菜单，菜单角色关联表
     *
     * @param info
     */
    @Override
    @Transactional
    public void deleteInfo(TMenu info) {
        //删除 菜单
        this.baseMapper.deleteById(info.getId());
        //删除菜单关联角色
        LambdaQueryWrapper<TRoleMenu> wrapper = new LambdaQueryWrapper<TRoleMenu>()
                .eq(TRoleMenu::getMenuId, info.getId().toString());
        roleMenuService.delete(wrapper);
    }


    private List<MenuTree<TMenu>> convertMenus(List<TMenu> menus) {
        List<MenuTree<TMenu>> trees = new ArrayList<>();
        menus.forEach(menu -> {
            MenuTree<TMenu> tree = new MenuTree<>();
            tree.setId(String.valueOf(menu.getId()));
            tree.setParentId(String.valueOf(menu.getParentId()));
            tree.setTitle(menu.getMenuName());
            tree.setIcon(menu.getIcon());
            tree.setData(menu);
            tree.setHref(menu.getUrl());
            trees.add(tree);
        });
        return trees;
    }

    private List<MenuFormSelect<TMenu>> convertFormSelectMenu(List<TMenu> menus) {
        List<MenuFormSelect<TMenu>> trees = new ArrayList<>();
        menus.forEach(menu -> {
            MenuFormSelect<TMenu> tree = new MenuFormSelect<>();
            tree.setId(String.valueOf(menu.getId()));
            tree.setParentId(menu.getParentId() + "");
            tree.setName(menu.getMenuName());
            tree.setData(menu);
            trees.add(tree);
        });
        return trees;
    }

}
