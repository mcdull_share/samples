package com.example.bm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.dao.model.TBank;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * 银行管理表 Service接口
 *
 * @author majian
 * @date 2019-11-11
 */
public interface BankService extends IService<TBank> {
    /**
     * 查询（自定义分页）
     *
     * @param params
     * @return Page<TBank>
     */
    Page<TBank> getPageList(Map<String, String> params, int pageNum, int pageSize);

    /**
    * 不分页
    *
    * @param params
    * @return
    */
    List<TBank> getList(Map<String, String> params);

    /**
     * 查询（所有）
     *
     * @param tBank tBank
     * @return List<TBank>
     */
    List<TBank> findTBanks(TBank tBank);

    /**
     * 新增
     *
     * @param tBank tBank
     */
    void createTBank(TBank tBank);

    /**
     * 修改
     *
     * @param tBank tBank
     */
    void updateTBank(TBank tBank);

    /**
     * 删除
     *
     * @param tBank tBank
     */
    void deleteTBank(TBank tBank);
}
