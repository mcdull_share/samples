package com.example.bm.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.dao.model.TRole;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * UserService Service接口
 */
public interface RoleService extends IService<TRole> {

    /**
     * s
     * 查询列表, 自定义分页
     *
     * @param params
     * @return
     */
    Page<TRole> getPageList(Map<String, Object> params, int pageNum, int pageSize);

    /**
     * 不分页
     *
     * @param params
     * @return
     */
    List<TRole> getList(Map<String, Object> params);


    /**
     * 新增
     *
     * @param info
     */
    void createInfo(TRole info, Map<String, String> params);

    /**
     * 更新
     */
    void updateInfo(TRole info, Map<String, String> params);


    void deleteInfo(TRole user);
}
