package com.example.bm.generator.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.bm.generator.pojo.Column;
import com.example.bm.generator.pojo.Table;
import com.example.dao.model.TGeneratorConfig;

import java.util.List;
import java.util.Map;

/**
 * @author bryceM
 */
public interface IGeneratorConfigService extends IService<TGeneratorConfig> {

    /**
     * 查询
     *
     * @return GeneratorConfig
     */
    TGeneratorConfig findGeneratorConfig();

    /**
     * 修改
     *
     * @param generatorConfig generatorConfig
     */
    void updateGeneratorConfig(TGeneratorConfig generatorConfig);

    /**
     * 获取所有表
     *
     * @param params
     * @param pageNum
     * @param pageSize
     * @return
     */
    List<Table> getTables(Map<String, String> params, int pageNum, int pageSize);

    /**
     * 获取表字段
     *
     * @param tableName
     * @return
     */
    List<Column> getColumns(String tableName);

}
