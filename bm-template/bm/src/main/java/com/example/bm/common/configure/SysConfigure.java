package com.example.bm.common.configure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author bryceM
 */
@Configuration
@EnableSwagger2
public class SysConfigure {

//    @Autowired
//    private FebsProperties properties;

    // @Bean(FebsConstant.ASYNC_POOL)
    public ThreadPoolTaskExecutor asyncThreadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(20);
        executor.setQueueCapacity(200);
        executor.setKeepAliveSeconds(30);
        executor.setThreadNamePrefix("Febs-Async-Thread");
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setAwaitTerminationSeconds(60);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }

    /**
     * XssFilter Bean
     */
//    @Bean
//    public FilterRegistrationBean xssFilterRegistrationBean() {
//        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
//        filterRegistrationBean.setFilter(new XssFilter());
//        filterRegistrationBean.setOrder(1);
//        filterRegistrationBean.setEnabled(true);
//        filterRegistrationBean.addUrlPatterns("/*");
//        Map<String, String> initParameters = new HashMap<>();
//        initParameters.put("excludes", "/favicon.ico,/img/*,/js/*,/css/*");
//        initParameters.put("isIncludeRichText", "true");
//        filterRegistrationBean.setInitParameters(initParameters);
//        return filterRegistrationBean;
//    }

    /**
     * 创建API应用
     * apiInfo() 增加API相关信息
     * 通过select()函数返回一个ApiSelectorBuilder实例，指定扫描的包路径来定义要建立API的controller目录。
     *
     * @return
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                // 设置是否开启swagger,生产环境关闭
                .enable(true)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.bm"))
                .paths(PathSelectors.any())
                .build();
//                .globalOperationParameters(setHeaderToken());
    }

    /**
     * 创建该API的基本信息（这些基本信息会展现在文档页面中）
     * 访问地址：http://项目实际地址/swagger-ui.html
     * <p>
     * http://localhost:9090/example/swagger-ui.html
     *
     * @return
     */
    @SuppressWarnings("deprecation")
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("API文档")
                .description("接口请求说明：\r\n"
                        + "报文规范中各参数如果不可空，则必须填写，否则报错。\r\n"
                        + "参数值为空的字段，不出现在JSON串中，不参与签名。\r\n"
                        + "按参数首字母a-z排序(body中参数按相同方式排序)，一级参数全部参与签名，报文为JSON格式;\r\n"
                        + "签名算法：RSA/HMacSHA256，测试HMacSHA256-KEY：990617。\r\n"
                        + "测试环境APPID：1811011059060727510\r\n"
                        + "请求的时间戳，接入系统的时间误差不能超过10分钟，" +
                        "格式为：yyyy-MM-dd HH:mm:ss，如：2019-07-17 12:58:15")
                .termsOfServiceUrl("")
                .version("1.0.0")
                .contact("")
                .build();
    }

}
