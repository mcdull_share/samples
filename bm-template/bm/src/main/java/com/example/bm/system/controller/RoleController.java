package com.example.bm.system.controller;

import cn.hutool.core.convert.Convert;
import com.example.bm.common.controller.BaseController;
import com.example.bm.system.pojo.MenuTree;
import com.example.bm.system.service.MenuService;
import com.example.bm.system.service.RoleService;
import com.example.common.domain.Layui;
import com.example.common.exception.ServiceException;
import com.example.dao.model.TMenu;
import com.example.dao.model.TRole;
import com.github.pagehelper.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 用户controller
 */
@Slf4j
@Controller
@RequestMapping("/sys/role")
public class RoleController extends BaseController {

    @Autowired
    private RoleService roleService;
    @Autowired
    private MenuService menuService;

    /**
     * 用户列表
     *
     * @param modelAndView
     * @return
     */
    @GetMapping(value = "/index")
    public ModelAndView index(ModelAndView modelAndView) {
        modelAndView.setViewName("views/system/role/roleList");
        return modelAndView;
    }

    /**
     * 明细
     *
     * @param params
     * @return
     */
    @RequestMapping("/list")
    @ResponseBody
    public Layui<List<TRole>> queryList(@RequestParam Map<String, Object> params, HttpServletRequest request) {
        log.info("角色明细 queryList={}", params);
        try {
            int pageNum = Convert.toInt(params.get("page"));
            int pageSize = Convert.toInt(params.get("limit"));
            Page<TRole> list = roleService.getPageList(params, pageNum, pageSize);
            return Layui.success(list.getResult(), list.getTotal());
        } catch (ServiceException e) {
            log.info(e.getMessage());
            return Layui.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Layui.error();
    }


    /**
     * 添加
     *
     * @param params
     * @param request
     * @return
     */
    @RequestMapping("/add")
    @ResponseBody
    public Layui<String> add(@RequestParam Map<String, String> params, @ModelAttribute TRole user, HttpServletRequest request) {
        log.info("角色添加 params={}", params);
        try {
            roleService.createInfo(user, params);
            return Layui.success();
        } catch (ServiceException e) {
            log.info(e.getMessage());
            return Layui.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Layui.error();
    }


    /**
     * 编辑
     *
     * @param modelAndView
     * @return
     */
    @GetMapping(value = "/detailPage")
    public ModelAndView detailPage(ModelAndView modelAndView, @RequestParam Map<String, Object> params) {
        modelAndView.setViewName("views/system/role/roleDetail");
        //角色信息
        Long roleId = Convert.toLong(params.get("id"));
        TRole role = roleService.getById(roleId);
        modelAndView.addObject("role", role);

        //角色菜单
        List<MenuTree<TMenu>> list = menuService.getRoleMenuList(params);
        modelAndView.addObject("menuData", list);
        return modelAndView;
    }


    /**
     * 编辑
     *
     * @param modelAndView
     * @return
     */
    @GetMapping(value = "/editPage")
    public ModelAndView editPage(ModelAndView modelAndView, @RequestParam Map<String, Object> params) {
        modelAndView.setViewName("views/system/role/roleEdit");
        //角色信息
        Long roleId = Long.parseLong(params.get("id").toString());
        TRole role = roleService.getById(roleId);
        modelAndView.addObject("role", role);

        //角色菜单
        List<MenuTree<TMenu>> list = menuService.getRoleMenuList(params);
        modelAndView.addObject("menuData", list);
        return modelAndView;
    }

    /**
     * 添加
     *
     * @param params
     * @param request
     * @return
     */
    @RequestMapping("/edit")
    @ResponseBody
    public Layui<String> edit(@RequestParam Map<String, String> params, @ModelAttribute TRole user, HttpServletRequest request) {
        log.info("角色编辑 params={}", params);
        try {
            roleService.updateInfo(user, params);
            return Layui.success();
        } catch (ServiceException e) {
            log.info(e.getMessage());
            return Layui.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Layui.error();
    }


    /**
     * 删除
     *
     * @param params
     * @param request
     * @return
     */
    @RequestMapping("/del")
    @ResponseBody
    public Layui<String> del(@RequestParam Map<String, String> params, HttpServletRequest request) {
        log.info("角色编辑 params={}", params);
        try {
            TRole info = TRole.builder().id(Convert.toLong(params.get("id"))).build();
            roleService.deleteInfo(info);
            return Layui.success();
        } catch (ServiceException e) {
            log.info(e.getMessage());
            return Layui.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Layui.error();
    }
}
