package com.example.bm.system.controller;

import com.example.bm.system.pojo.MenuFormSelect;
import com.example.bm.system.pojo.MenuTree;
import com.example.bm.system.service.MenuService;
import com.example.common.domain.Layui;
import com.example.common.exception.ServiceException;
import com.example.dao.model.TMenu;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 用户controller
 */
@Slf4j
@Controller
@RequestMapping("/sys/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    /**
     * 列表
     *
     * @param modelAndView
     * @return
     */
    @GetMapping(value = "/index")
    public ModelAndView index(ModelAndView modelAndView) {
        modelAndView.setViewName("views/system/menu/menuList");
        return modelAndView;
    }

    /**
     * 明细
     *
     * @param params
     * @return
     */
    @RequestMapping("/list")
    @ResponseBody
    public Layui<List<MenuTree<TMenu>>> list(@RequestParam Map<String, Object> params, HttpServletRequest request) {
        log.info("菜单明细 queryList={}", params);
        try {
            List<MenuTree<TMenu>> list = menuService.getMenuTree(params);
            return Layui.success(list);
        } catch (ServiceException e) {
            log.info(e.getMessage());
            return Layui.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Layui.error();
    }

    /**
     * 明细
     *
     * @param params
     * @return
     */
    @RequestMapping("/listSelect")
    @ResponseBody
    public List<MenuFormSelect<TMenu>> listSelect(@RequestParam Map<String, Object> params, HttpServletRequest request) {
        log.info("菜单明细 listSelect queryList={}", params);
        try {
            return menuService.getFormSelectMenu(params);
        } catch (ServiceException e) {
            log.info(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }


    /**
     * 添加
     *
     * @param params
     * @param request
     * @return
     */
    @RequestMapping("/saveOrUpdate")
    @ResponseBody
    public Layui<String> saveOrUpdate(@RequestParam Map<String, String> params, @ModelAttribute TMenu info, HttpServletRequest request) {
        log.info("菜单添加/编辑 params={}", params);
        try {
            String saveOrUpdate = params.getOrDefault("saveOrUpdate", "save");
            if ("save".equals(saveOrUpdate)){
                menuService.createInfo(info);
            }else {
                menuService.updateInfo(info);
            }
            return Layui.success();
        } catch (ServiceException e) {
            log.info(e.getMessage());
            return Layui.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Layui.error();
    }

    /**
     * 添加
     *
     * @param params
     * @param request
     * @return
     */
    @RequestMapping("/del")
    @ResponseBody
    public Layui<String> del(@RequestParam Map<String, String> params, @ModelAttribute TMenu info, HttpServletRequest request) {
        log.info("菜单/按钮删除 params={}", params);
        try {
            menuService.deleteInfo(info);
            return Layui.success();
        } catch (ServiceException e) {
            log.info(e.getMessage());
            return Layui.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Layui.error();
    }


    /**
     * 编辑
     *
     * @param modelAndView
     * @return
     */
    @GetMapping(value = "/editPage")
    public ModelAndView editPage(ModelAndView modelAndView) {
        modelAndView.setViewName("views/system/menu/menuEdit");
        return modelAndView;
    }

    /**
     * 添加
     *
     * @param params
     * @param request
     * @return
     */
    @RequestMapping("/edit")
    @ResponseBody
    public Layui<String> edit(@RequestParam Map<String, Object> params, @ModelAttribute TMenu info, HttpServletRequest request) {
        log.info("菜单编辑 params={}", params);
        try {
            menuService.updateInfo(info);
            return Layui.success();
        } catch (ServiceException e) {
            log.info(e.getMessage());
            return Layui.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Layui.error();
    }

}
