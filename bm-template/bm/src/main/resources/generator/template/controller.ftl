package cc.mrbird.febs.system.controller;

import ${basePackage}.${entityPackage}.${className};
import ${basePackage}.${servicePackage}.I${className}Service;
import com.example.common.exception.ServiceException;
import com.github.pagehelper.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * ${tableComment} Controller
 *
 * @author ${author}
 * @date ${date}
 */
@Slf4j
@Controller("/${className}")
@Controller("/${className}")
public class ${className}Controller extends BaseController {

    @Autowired
    private I${className}Service ${className?uncap_first}Service;


    /**
    * 首页
    *
    * @param modelAndView
    * @return
    */
    @GetMapping(value = "/index")
    public ModelAndView index(ModelAndView modelAndView) {
        modelAndView.setViewName("views/${className?uncap_first}/${className?uncap_first}List");
        return modelAndView;
    }

    /**
    * 明细
    *
    * @param params
    * @return
    */
    @PostMapping("/list")
    @ResponseBody
    public Layui<List<${className}>> list(@RequestParam Map<String, String> params, HttpServletRequest request) {
        log.info("${className}明细 list={}", params);
        try {
            int pageNum = Integer.parseInt(String.valueOf(params.get("page")));
            int pageSize = Integer.parseInt((String) params.get("limit"));
            Page<${className}> list = ${className?uncap_first}Service.getPageList(params, pageNum, pageSize);
            return Layui.success(list.getResult(), list.getTotal());
        } catch (ServiceException e) {
            log.info(e.getMessage());
            return Layui.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Layui.error();
    }

    /**
    * 查询所有
    *
    * @param params
    * @return
    */
    @RequestMapping("/listAll")
    @ResponseBody
    public Layui<List<${className}>> listAll(@RequestParam Map<String, String> params, HttpServletRequest request) {
        log.info("查询所有 queryList={}", params);
        try {
            List<${className}> list = ${className?uncap_first}Service.getBaseMapper().selectList(null);
            return Layui.success(list, (long) list.size());
        } catch (ServiceException e) {
            log.info(e.getMessage());
            return Layui.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return Layui.error();
    }

    /**
    * 新增
    *
    * @param modelAndView
    * @return
    */
    @GetMapping(value = "/addPage")
    public ModelAndView index(ModelAndView modelAndView) {
        modelAndView.setViewName("views/${className?uncap_first}/${className?uncap_first}Add");
        return modelAndView;
    }

}
