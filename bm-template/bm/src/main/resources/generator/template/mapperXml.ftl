<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${basePackage}.${mapperPackage}.dao.mapper.${className}Mapper">

    <resultMap id="${className?uncap_first}" type="${basePackage}.${entityPackage}.dao.${className}">
        <#if columns??>
        <#list columns as column>
        <#if column.isKey = true>
        <id column="${column.name}" jdbcType="${column.type?upper_case}" property="${column.field?uncap_first}"/>
        <#elseif column.type == 'datetime'>
            <result column="${column.name}" jdbcType="TIMESTAMP" property="${column.field?uncap_first}"/>
        <#elseif column.type == 'int'>
            <result column="${column.name}" jdbcType="INTEGER" property="${column.field?uncap_first}"/>
        <#else>
        <result column="${column.name}" jdbcType="${column.type?upper_case}" property="${column.field?uncap_first}"/>
        </#if>
        </#list>
        </#if>
    </resultMap>

    <sql id="base_column"><![CDATA[
        <#list columns as column>
            ${column.name}<#if column_has_next>,</#if>
        </#list>
    ]]></sql>

    <sql id="select_where">
        <where>
            <#if columns??>
                <#list columns as column>
            <#if column.name = "CREATE_USER" || column.name = "CREATE_TIME" || column.name = "LAST_UPDATE_USER"
                || column.name = "LAST_UPDATE_TIME">
            <#elseif  column.name?ends_with('NAME')>
            <if test="${column.field?uncap_first} != null and ${column.field?uncap_first} != ''">
                 and ${column.name} like CONCAT('%, <#noparse>#{</#noparse>${column.field?uncap_first}<#noparse>}</#noparse>, '%')
            </if>
            <#else >
            <if test="${column.field?uncap_first} != null and ${column.field?uncap_first} != ''">
                and ${column.name} = <#noparse>#{</#noparse>${column.field?uncap_first}<#noparse>}</#noparse>
            </if>
            </#if>
                </#list>
            </#if>
        </where>
    </sql>

    <select id="findList" parameterType="java.util.Map" resultType="${basePackage}.${entityPackage}.dao.${className}">
        select
        <include refid="base_column"/>
        from ${tableName}
        <include refid="select_where"/>
        order by CREATE_TIME desc
    </select>

</mapper>
