package ${basePackage}.${mapperPackage};

import ${basePackage}.${entityPackage}.${className};
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * ${tableComment} Mapper
 *
 * @author ${author}
 * @date ${date}
 */
public interface ${className}Mapper extends BaseMapper<${className}> {

    /**
    * 自定义分页
    * @param params
    * @return
    */
    List<${className}> findList(Map<String, String> params);


}
