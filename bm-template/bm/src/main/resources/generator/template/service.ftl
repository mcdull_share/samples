package ${basePackage}.${servicePackage};

import ${basePackage}.${entityPackage}.${className};
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * ${tableComment} Service接口
 *
 * @author ${author}
 * @date ${date}
 */
public interface I${className}Service extends IService<${className}> {
    /**
     * 查询（自定义分页）
     *
     * @param params
     * @return Page<${className}>
     */
    Page<${className}> getPageList(Map<String, String> params, int pageNum, int pageSize);

    /**
    * 不分页
    *
    * @param params
    * @return
    */
    List<${className}> getList(Map<String, String> params);

    /**
     * 查询（所有）
     *
     * @param ${className?uncap_first} ${className?uncap_first}
     * @return List<${className}>
     */
    List<${className}> find${className}s(${className} ${className?uncap_first});

    /**
     * 新增
     *
     * @param ${className?uncap_first} ${className?uncap_first}
     */
    void create${className}(${className} ${className?uncap_first});

    /**
     * 修改
     *
     * @param ${className?uncap_first} ${className?uncap_first}
     */
    void update${className}(${className} ${className?uncap_first});

    /**
     * 删除
     *
     * @param ${className?uncap_first} ${className?uncap_first}
     */
    void delete${className}(${className} ${className?uncap_first});
}
