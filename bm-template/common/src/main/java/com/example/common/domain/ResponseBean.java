package com.example.common.domain;

import com.example.common.enums.SignEnum;
import com.example.common.enums.SystemEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "返回结果")
public class ResponseBean<T> implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "响应码 0000:成功 其他:失败", name = "code")
	@Builder.Default
	private String code = "0000";

	@ApiModelProperty(value = "响应信息", name = "msg")
	@Builder.Default
	private String msg = "执行成功";

	@ApiModelProperty(value = "返回内容，服务调用成功后的返回结果，是一个嵌套的String类型的JSON对象", name = "body")
	private T body;

	@ApiModelProperty(value = "请求流水号", name = "reqNo")
	private String reqNo;

	@ApiModelProperty(value = "签名类型 1:RSA 2:HMacSHA256", name = "signType")
    private String signType;

	@ApiModelProperty(value = "签名字符串", name = "signValue")
	private String signValue;

	@Override
	public String toString() {
		return "ResponseEntity [code=" + code + ", msg=" + msg + ", body=" + body + ", reqNo=" + reqNo + ", signType="
				+ signType + ", signValue=" + signValue + "]";
	}

	/**
	 * 请求成功
	 * @param sysRlt
	 * @param result
	 * @return
	 */
	public static <T> ResponseBean<T> success(SystemEnum sysRlt, T result, String reqNo) {
		ResponseBean<T> success = new ResponseBean<T>();
		success.setMsg(sysRlt.getMessage());
		success.setCode(sysRlt.getCode());
		success.setBody(result);
		success.setReqNo(reqNo);
		success.setSignType(SignEnum.HMacSHA256.getValue());
		String signValue = setSignHMacSHA256(sysRlt, result, reqNo);
		success.setSignValue(signValue);
		return success;
	}

	/**
	 * 请求成功
	 * @param success
	 * @param result
	 */
	public static <T> void success(ResponseBean<T> success, T result, String reqNo) {
		success.setMsg(SystemEnum.SUCCESS.getMessage());
		success.setCode(SystemEnum.SUCCESS.getCode());
		success.setBody(result);
		success.setReqNo(reqNo);
		success.setSignType(SignEnum.HMacSHA256.getValue());
		String signValue = setSignHMacSHA256(SystemEnum.SUCCESS, result, reqNo);
		success.setSignValue(signValue);
	}

	/**
	 * 请求成功
	 * @param result
	 * @return
	 */
	public static <T> ResponseBean<T> success(T result, String reqNo) {
		ResponseBean<T> success = new ResponseBean<T>();
		success.setMsg(SystemEnum.SUCCESS.getMessage());
		success.setCode(SystemEnum.SUCCESS.getCode());
		success.setBody(result);
		success.setReqNo(reqNo);
		success.setSignType(SignEnum.HMacSHA256.getValue());
		String signValue = setSignHMacSHA256(SystemEnum.SUCCESS, result, reqNo);
		success.setSignValue(signValue);
		return success;
	}

	/**
	 * 返回数据加密
	 * @param sysRlt 返回状态
	 * @param body 返回数据
	 * @param reqNo 请求流水号
	 * @return 加密结果
	 */
	private static <T> String setSignHMacSHA256(SystemEnum sysRlt, T body, String reqNo) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("code", sysRlt.getCode()+"");
		paramMap.put("message", sysRlt.getMessage());
		paramMap.put("reqNo", reqNo);
		paramMap.put("signType", SignEnum.HMacSHA256.getValue());

		// 按Key进行排序
		//paramMap = ParameterUtils.sortMapByKey(paramMap);
		// 参数加密
		//String signValue = SecurityUtil.HMacSHA256(JSONUtils.object2Json(paramMap), Constant.HMacSHA256_KEY);
		return null;
	}

	/**
	 * 请求成功
	 * @return
	 */
	public static <T> ResponseBean<T> success(String reqNo) {
		ResponseBean<T> success = new ResponseBean<T>();
		success.setMsg(SystemEnum.SUCCESS.getMessage());
		success.setCode(SystemEnum.SUCCESS.getCode());
		success.setBody(null);
		success.setReqNo(reqNo);
		success.setSignType(SignEnum.HMacSHA256.getValue());
		String signValue = setSignHMacSHA256(SystemEnum.SUCCESS, null, reqNo);
		success.setSignValue(signValue);
		return success;
	}

	/**
	 * 请求成功（免校验）
	 * @return
	 */
	public static <T> ResponseBean<T> retSuccess(T data) {
		ResponseBean<T> success = new ResponseBean<T>();
		success.setMsg(SystemEnum.SUCCESS.getMessage());
		success.setCode(SystemEnum.SUCCESS.getCode());
		success.setBody(data);
		return success;
	}

	/**
	 * 校验成功
	 * @return
	 */
	public static <T> ResponseBean<T> checkSuccess(String reqNo) {
		ResponseBean<T> success = new ResponseBean<T>();
		success.setMsg(SystemEnum.SUCCESS_FLAG.getMessage());
		success.setCode(SystemEnum.SUCCESS_FLAG.getCode());
		success.setBody(null);
		success.setReqNo(reqNo);
		success.setSignType(SignEnum.HMacSHA256.getValue());
		String signValue = setSignHMacSHA256(SystemEnum.SUCCESS_FLAG, null, reqNo);
		success.setSignValue(signValue);
		return success;
	}

	/**
	 * 请求失败
	 * @param sysRlt
	 * @return
	 */
	public static <T> ResponseBean<T> error(SystemEnum sysRlt, String reqNo) {
		ResponseBean<T> error = new ResponseBean<T>();
		error.setMsg(sysRlt.getMessage());
		error.setCode(sysRlt.getCode());
		error.setBody(null);
		error.setReqNo(reqNo);
		error.setSignType(SignEnum.HMacSHA256.getValue());
		String signValue = setSignHMacSHA256(sysRlt, null, reqNo);
		error.setSignValue(signValue);
		return error;
	}

	/**
	 * 请求失败
	 * @param sysRlt
	 * @return
	 */
	public static <T> ResponseBean<T> error(String code, String msg) {
		ResponseBean<T> error = new ResponseBean<T>();
		error.setMsg(msg);
		error.setCode(code);
		error.setBody(null);
		return error;
	}

	/**
	 * 请求失败
	 * @param sysRlt
	 * @return
	 */
	public static <T> ResponseBean<T> error(SystemEnum sysRlt) {
		ResponseBean<T> error = new ResponseBean<T>();
		error.setMsg(sysRlt.getMessage());
		error.setCode(sysRlt.getCode());
		return error;
	}

	/**
	 * 请求失败
	 * @param sysRlt
	 * @return
	 */
	public static <T> ResponseBean<T> error(SystemEnum sysRlt, String msg, String reqNo) {
		ResponseBean<T> error = new ResponseBean<T>();
		error.setMsg(msg);
		if (sysRlt != null) {
			error.setCode(sysRlt.getCode());
		}
		error.setBody(null);
		error.setReqNo(reqNo);
		error.setSignType(SignEnum.HMacSHA256.getValue());
		String signValue = setSignHMacSHA256(sysRlt, null, reqNo);
		error.setSignValue(signValue);
		return error;
	}

	/**
	 * 请求失败
	 * @return
	 */
	public static <T> ResponseBean<T> error(String reqNo) {
		ResponseBean<T> error = new ResponseBean<T>();
		error.setMsg(SystemEnum.FAILURE.getMessage());
		error.setCode(SystemEnum.FAILURE.getCode());
		error.setBody(null);
		error.setReqNo(reqNo);
		error.setSignType(SignEnum.HMacSHA256.getValue());
		String signValue = setSignHMacSHA256(SystemEnum.FAILURE, null, reqNo);
		error.setSignValue(signValue);
		return error;
	}

	/**
	 * 请求失败
	 * @return
	 */
	public static <T> ResponseBean<T> error(T result, String reqNo) {
		ResponseBean<T> error = new ResponseBean<T>();
		error.setMsg(SystemEnum.FAILURE.getMessage());
		error.setCode(SystemEnum.FAILURE.getCode());
//        String body = JSONUtils.object2Json(result);
		error.setBody(result);
		error.setReqNo(reqNo);
		error.setSignType(SignEnum.HMacSHA256.getValue());
		String signValue = setSignHMacSHA256(SystemEnum.FAILURE, result, reqNo);
		error.setSignValue(signValue);
		return error;
	}

	/**
	 * 校验失败
	 * @return
	 */
	public static <T> ResponseBean<T> checkError(String reqNo) {
		ResponseBean<T> error = new ResponseBean<T>();
		error.setMsg(SystemEnum.FAILURE.getMessage());
		error.setCode(SystemEnum.FAILURE.getCode());
		error.setBody(null);
		error.setReqNo(reqNo);
		error.setSignType(SignEnum.HMacSHA256.getValue());
		String signValue = setSignHMacSHA256(SystemEnum.FAILURE, null, reqNo);
		error.setSignValue(signValue);
		return error;
	}

}
