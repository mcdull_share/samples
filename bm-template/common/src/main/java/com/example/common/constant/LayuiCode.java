package com.example.common.constant;

public class LayuiCode {

    /***************************正确响应*************************/
    public static final String CODE_SUCC = "0000";
    public static final String MSG_SUCC = "操作成功";

    /***************************错误响应*************************/
    public static final String CODE_FAIL = "9999";
    public static final String MSG_FAIL = "系统异常！";

    /***************************系统管理模块错误响应*************************/
    public static final String MSG_ADD_SUCC = "添加成功！";
    public static final String MSG_DEL_SUCC = "删除成功！";

    public static final String CODE_SAVE_USER_FAIL = "1000";
    public static final String MSG_SAVE_USER_FAIL = "该用户已存在！";

    public static final String CODE_ROLE_USER_FAIL = "1001";
    public static final String MSG_ROLE_USER_FAIL = "该角色关联了用户，不能删除！";


    public static final String CODE_PASSWORD_NOTEQUEL_FAIL = "1002";
    public static final String MSG_PASSWORD_NOTEQUEL_FAIL = "新密码与确认密码不一致！";

    public static final String CODE_PASSWORD_NULL = "1003";
    public static final String MSG_PASSWORD_NULL = "密码不能为空！";

    public static final String CODE_USER_NOT_LOGIN = "1004";
    public static final String MSG_USER_NOT_LOGIN = "用户未登陆！";

    public static final String CODE_PASSWORD_WRONG = "1005";
    public static final String MSG_PASSWORD_WRONG = "用户原密码错误！";
}
