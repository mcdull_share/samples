package com.example.common.exception;


import com.example.common.enums.SystemEnum;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 服务（业务）异常如“ 账号或密码错误 ”，该异常只做INFO级别的日志记录 @see WebMvcConfigurer
 */
@NoArgsConstructor
@Getter
@Setter
public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private String code;

    public ServiceException(String code, String msg) {
        super(msg);
        this.code = code;
    }

    public ServiceException(SystemEnum type) {
        super(type.getMessage());
        this.code = type.getCode();
    }

    public ServiceException(String msg) {
        super(msg);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
