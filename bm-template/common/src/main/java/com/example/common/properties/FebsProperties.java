package com.example.common.properties;

import lombok.Data;

/**
 * @author bryceM
 */
@Data
//@SpringBootConfiguration
//@PropertySource(value = {"classpath:febs.properties"})
//@ConfigurationProperties(prefix = "febs")
public class FebsProperties {

    private ShiroProperties shiro = new ShiroProperties();
    private boolean autoOpenBrowser = true;
    private String[] autoOpenBrowserEnv = {};
    private SwaggerProperties swagger = new SwaggerProperties();

    private int maxBatchInsertNum = 1000;
}
