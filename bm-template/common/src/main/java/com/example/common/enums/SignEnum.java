package com.example.common.enums;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 加密类型枚举类
 */
@Getter
@NoArgsConstructor
public enum SignEnum {

    RSA("1"),
    HMacSHA256("2"),
    TRIPLE_DES("3"),
    ;

    SignEnum(String value) {
        this.value = value;
    }

    private String value;
}
