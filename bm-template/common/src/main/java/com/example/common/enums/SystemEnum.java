package com.example.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author gangwei.chen
 * @version V1.0
 * @ClassName: SystemEnum
 * @Description: 系统错误枚举 定义系统常用错误，value为枚举值，message为该枚举的错误消息
 * @date 2019年6月25日上午15:23:46
 */
@Getter
@AllArgsConstructor
public enum SystemEnum {
    /**
     * 基础模块相关
     **/
    FAILURE("9001", "请求处理失败"),
    SUCCESS("0000", "请求处理成功"),
    AUTHEN_FAILED("0001", "签名无效"),
    LIIEGAL_USER_REQUEST("0002", "接口不存在"),
    REQUEST_OVERDUE("0003", "请求已过期"),
    ENCRY_FAILED("0004", "加密失败"),
    DECRY_FAILED("0005", "解密失败"),
    APPID_INVALID("0006", "AppId无效"),
    TYPE_ERROR("0007", "类型错误"),
    PARAMETER_MISSING("0008", "参数缺失"),
    SYSTEM_SELECT_NULL("0009", "查询数据为空"),
    DATE_PATTERN_ERROR("0010", "日期格式错误"),
    DATE_CHECK_ERROR("0011", "起始时间不能大大于结束日期"),
    EMPTY_ERROR("0012", "数据为空"),
    AMOUNT_ERROR("0013", "金额格式错误"),

    /**
     * 参数校验通过
     */
    SUCCESS_FLAG("1000", "ok"),
    FAILURE_FLAG("1001", "请求参数校验未通过"),

    /**
     * 用户相关
     **/
    USERNAME_FAILED("2001", "用户名称不能为空"),
    USERCARD_FAILED("2002", "用户证件号不能为空"),
    USERCARD_FORMART_FAILED("2003", "身份证号格式不正确"),
    PHONENO_FAILED("2004", "手机号不能为空"),
    PHONENO_FORMART_FAILED("2005", "手机号格式不正确"),
    PASSWORD_FAILED("2006", "密码不能为空"),
    PHONE_PASSWORD_FAILED("2007", "手机或用户名密码错误"),

    /**
     * 代理商相关
     **/
    AGENTNO_FAILED("3001", "代理商编号不能为空"),
    AGENTNO_NOTEXIST_FAILED("3002", "代理商编号不存在"),
    AGENT_ADD_FAILED("3003", "添加代理商失败"),
    AGENT_STATISTICS_FAILED("3004", "无统计信息活代理商编号不存在"),
    AGENT_CASHACCT_NOTEXIST("3005", "代理商现金账户信息不存在"),
    AGENT_WAITSETTLEACCT_NOTEXIST("3006", "代理商待结算账户信息不存在"),
    AGENTNO_CHANNEL_FAILED("3007", "代理商渠道标识不正确"),

    /**
     * 产品相关
     **/
    PRODUCTID_FAILED("4001", "产品ID不能为空"),
    REPEAT_SET_PRODUCT("4002", "请勿重复配置"),
    PRODUCT_TYPE_FAILED("4003", "产品类型不能为空"),

    /**
     * 对账相关
     **/
    BANK_SUPPORT_FAILED("5001", "该银行对账单暂不支持"),
    BANK_ORDER_FORMAT_FAILED("5002", "对账单数据格式不正确"),
    FILE_PARSE_FAILED("5003", "文件解析失败"),
    ORDER_STATUS_FAILED("5004", "撤回状态不正确"),
    VOUCHER_NO_FAILED("5005", "对账单处理凭证为空"),
    COMMISION_TYPE_FAILED("5006", "佣金类型不正确"),
    CHECK_ORDER_MONEY_FAILED("5007", "对账单放款金额不正确"),

    /**
     * 账户相关
     */
    WITHDRAW_FAILED("5101", "提现失败"),

    /**
     * 产品申请相关
     **/
    REPEAT_DO_PRODUCTID_FAILED("6001", "该产品您已申请过，请勿重复申请！"),
    ORG_PRODUCTID_FAILED("6002", "产品信息不完整"),
    ORDER_TYPE_ERROR("6003", "产品类型错误"),


    /**
     * 交易相关
     */
    ROUTER_EMPTY("7001", "未查到可用路由"),
    INSUFFICIENT_AMOUNT("7002", "余额不足"),
    INSUFFICIENT_FEE("7003", "余额不足扣除手续费"),

    /**
     * 系统相关
     **/
    OTHER_ERROR("9000", "其他错误"),
    UNKNOW_ERROR("9100", "未知错误"),
    DATABASE_ERROR("9200", "数据库错误"),
    EXTERNAL_ERROR("9300", "未知错误"),
    REQUEST_TIMEOUT("9400", "请求超时"),
    SYSTEM_WH("9500", "系统维护中"),
    REPEAT_DO("9600", "重复操作"),
    NOT_FOUND_RESOURCE("9700", "未找到资源"),
    SYSTEM_EXCEPTION("9999", "系统异常");

    private final String code;
    private final String message;

    /**
     * 处理成功状态码
     *
     * @return
     */
    public static String success() {
        return SUCCESS.getCode();
    }

    /**
     * 参数校验成功状态码
     *
     * @return
     */
    public static String successFlag() {
        return SUCCESS_FLAG.getCode();
    }

    /**
     * 处理失败状态码
     *
     * @return
     */
    public static String fail() {
        return FAILURE.getCode();
    }

    /**
     * 参数校验失败状态码
     *
     * @return
     */
    public static String failFlag() {
        return FAILURE_FLAG.getCode();
    }
}

