package com.example.common.domain;

import com.example.common.constant.LayuiCode;
import com.example.common.enums.SystemEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * layui 返回bean
 *
 * @param <T>
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Layui<T> {

    private String code;
    private String msg;
    private Long count;
    private T data;

    public Layui(SystemEnum sysEnum) {
        this.code = sysEnum.getCode();
        this.msg = sysEnum.getMessage();
    }

    /**
     * 成功返回
     */
    public static <T> Layui<T> success(){
        return Layui.<T>builder().code(LayuiCode.CODE_SUCC).msg(LayuiCode.MSG_SUCC).data(null).count(null).build();
    }

    public static <T> Layui<T> success(T data){
        return Layui.<T>builder().code(LayuiCode.CODE_SUCC).msg(LayuiCode.MSG_SUCC).data(data).count(null).build();
    }


    public static <T> Layui<T> success(T data, Long count){
        return Layui.<T>builder().code(LayuiCode.CODE_SUCC).msg(LayuiCode.MSG_SUCC).data(data).count(count).build();
    }

    /**
     * 失败返回
     * @param <T>
     * @return
     */
    public static <T> Layui<T> error(){
        return Layui.<T>builder().code(LayuiCode.CODE_FAIL).msg(LayuiCode.MSG_FAIL).data(null).count(null).build();
    }

    public static <T> Layui<T> error(String msg){
        return Layui.<T>builder().code(LayuiCode.CODE_FAIL).msg(msg).data(null).count(null).build();
    }

    public static <T> Layui<T> error(String code, String msg){
        return Layui.<T>builder().code(code).msg(msg).data(null).count(null).build();
    }

    public static <T> Layui<T> error(SystemEnum systemEnum){
        return Layui.<T>builder().code(systemEnum.getCode()).msg(systemEnum.getMessage()).data(null).count(null).build();
    }


}
