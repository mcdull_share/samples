package com.example.biz.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @Description: 加密类型枚举类
 * @author gangwei.chen
 * @date Jun 26, 2019 5:39:11 PM
 * @version V1.0
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum UserTypeEnum {

	UNKNOWN(0,"未知"),
	ZB_ADMIN(1,"总部管理员"),
	FGS_ADMIN(2, "分公司管理员"),
	AGENT_ADMIN(3, "分公司管理员"),
	;

	private Integer type ;
	private String value ;


	public static String getValue(Integer type) {
		if (type == null){
			return UNKNOWN.value;
		}
		for (UserTypeEnum name : UserTypeEnum.values()) {
			if (name.getType().equals(type)) {
				return name.value;
			}
		}
		return UNKNOWN.value;
	}
}
