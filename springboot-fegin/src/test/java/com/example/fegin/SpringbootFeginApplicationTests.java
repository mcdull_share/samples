package com.example.fegin;

import com.example.fegin.service.DoubanBookService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest(classes = SpringbootFeginApplication.class)
public class SpringbootFeginApplicationTests {

	@Autowired
	private DoubanBookService doubanBookService;

	@Test
	void contextLoads() {
		doubanBookService.getBookInfo();
	}

}
