package com.example.fegin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SpringbootFeginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootFeginApplication.class, args);
	}

}
