package com.example.fegin.service;

import com.example.fegin.feginclient.DoubanBookApiClient;
import org.springframework.stereotype.Component;

/**
 * 豆瓣图书api服务
 */
@Component
public class DoubanBookService {

    private final DoubanBookApiClient doubanBookApiClient;

    public DoubanBookService(DoubanBookApiClient doubanBookApiClient) {
        this.doubanBookApiClient = doubanBookApiClient;
    }

    public void getBookInfo(){
        String bookId = "1003078";
        doubanBookApiClient.getBookInfo(bookId);
    }
}
