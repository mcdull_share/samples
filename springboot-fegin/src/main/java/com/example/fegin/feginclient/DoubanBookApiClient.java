package com.example.fegin.feginclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Component
@FeignClient(name = "douban-book-client",url = "https://douban.uieee.com")
public interface DoubanBookApiClient {

    @PostMapping("/v2/book/{bookId}")
    String getBookInfo(@PathVariable("bookId") String bookId);
}