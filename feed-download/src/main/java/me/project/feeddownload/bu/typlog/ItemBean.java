package me.project.feeddownload.bu.typlog;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@XStreamAlias("rss")
public class ItemBean {

    @XStreamAsAttribute
    final String xmlns = "http://www.example.org";

    @XStreamAsAttribute
    @XStreamAlias("xmlns:itunes")
    final String itunes = "http://www.itunes.com/dtds/podcast-1.0.dtd";

    @XStreamAlias(value = "channel")
    private List<Item> itemList;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @XStreamAlias("item")
    public static class Item {

        @XStreamAlias("enclosure")
        private Enclosure enclosure;

        @XStreamAlias("title")
        private String title;

        @XStreamAlias("guid")
        private String guid;
        @XStreamAlias("link")
        private String link;

        @XStreamAlias("itunes:author")
        private String author;

        @XStreamAlias("itunes:subtitle")
        private String subtitle;

        @XStreamAlias("itunes:duration")
        private String duration;

        @XStreamAlias("itunes:explicit")
        private String explicit;

        @XStreamAlias("itunes:season")
        private String season;

        @XStreamAlias("itunes:episode")
        private String episode;

        @XStreamAlias("itunes:episodeType")
        private String episodeType;

        @XStreamAlias("itunes:image")
        private String image;

        @XStreamAlias("itunes:description")
        private String description;

        @XStreamAlias("itunes:summary")
        private String summary;

        @XStreamAlias("itunes:encoded")
        private String encoded;

        @XStreamAlias("pubDate")
        private String pubDate;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @XStreamAlias("enclosure")
    public static class Enclosure{

        @XStreamAsAttribute
        private String url;

        @XStreamAsAttribute
        private String length;

        @XStreamAsAttribute
        private String type;

    }
}
