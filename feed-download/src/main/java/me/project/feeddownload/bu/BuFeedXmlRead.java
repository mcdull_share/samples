package me.project.feeddownload.bu;


import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileWriter;
import cn.hutool.core.util.XmlUtil;
import com.alibaba.excel.EasyExcel;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.Dom4JDriver;
import me.project.feeddownload.bu.typlog.ItemBean;
import me.project.feeddownload.bu.typlog.OutExcelBean;
import org.w3c.dom.Document;

import javax.xml.xpath.XPathConstants;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * bitchUp
 */
public class BuFeedXmlRead {

    public static void readXml() {
        //当前项目路径
        String projectPath = System.getProperty("user.dir");
        String filePath = projectPath + File.separator + "feed-download" + File.separator + "data" + File.separator;
        String fileName = "BitchupFMFeed.xml";
        System.out.println("读取文件：" + filePath + fileName);
        File xmlFile = FileUtil.file(filePath + fileName);
        Document docResult = XmlUtil.readXML(xmlFile);
        Object value = XmlUtil.getByXPath("//rss/channel/item", docResult, XPathConstants.STRING);
        System.out.println(value);
    }

    /**
     * item
     */
    private static ItemBean parseItemXml(String filePath) {
        File xmlFile = FileUtil.file(filePath);
        XStream xstream = new XStream(new Dom4JDriver());
        xstream.processAnnotations(ItemBean.class);
        XStream.setupDefaultSecurity(xstream);
        xstream.allowTypes(new Class[]{ItemBean.class, ItemBean.Item.class});
        ItemBean bo = (ItemBean) xstream.fromXML(xmlFile);
        System.out.println(bo.getItemList().size());
        return bo;
    }

    private static void writeToTxt(String filePath) {
        ItemBean itemBean = BuFeedXmlRead.parseItemXml(filePath);
        StringBuilder content = new StringBuilder();
        itemBean.getItemList().forEach(item -> {
            String tile = item.getTitle().trim();
            String url = item.getEnclosure().getUrl().trim();
            content.append(tile).append("\n").append(url).append("\n\n");
        });
        //写入文件
        String fileNameOut = "BitchupFMFeedOut_" + System.currentTimeMillis() + ".txt";
        System.out.println("写入文件，" + fileNameOut);
        FileWriter writer = new FileWriter(filePath + fileNameOut);
        writer.write(content.toString());
        System.out.println("写入完成");
    }

    private static void writeToExcel(String filePath) {
        ItemBean itemBean = BuFeedXmlRead.parseItemXml(filePath);
        List<OutExcelBean> content = new ArrayList<>();
        itemBean.getItemList().forEach(item -> {
            OutExcelBean bean = OutExcelBean.builder()
                    .title(item.getTitle().trim())
                    .description(item.getDescription().trim())
                    .summary(item.getSummary().trim())
                    .duration(item.getDuration().trim())
                    .pubDate(item.getPubDate())
                    .url(item.getEnclosure().getUrl())
                    .build();
            content.add(bean);
        });
        //写入文件
        String fileNameOut = "BitchupFMFeedOut_" + System.currentTimeMillis() + ".xlsx";
        System.out.println("写入文件，" + fileNameOut);
        // 写法1
        // 这里 需要指定写用哪个class去写，然后写到第一个sheet，名字为模板 然后文件流会自动关闭
        // 如果这里想使用03 则 传入excelType参数即可

        EasyExcel.write(filePath + fileNameOut, OutExcelBean.class).sheet("模板").doWrite(content);
        System.out.println("写入完成");
    }


    public static void main(String[] args) {
        String projectPath = System.getProperty("user.dir");
        String filePath = projectPath + File.separator + "feed-download" + File.separator + "data" + File.separator + "bu" + File.separator;
        String fileName = "BitchupFMFeed.xml";
        System.out.println("读取文件：" + filePath + fileName);

//        writeToTxt(filePath + fileName);
        writeToExcel(filePath + fileName);

    }

    public static String replace(String str) {
        String destination = "";
        if (str != null) {
            Pattern p = Pattern.compile("\\s*|\t|\r|\n");
            Matcher m = p.matcher(str);
            destination = m.replaceAll("");
        }
        return destination;
    }
}
