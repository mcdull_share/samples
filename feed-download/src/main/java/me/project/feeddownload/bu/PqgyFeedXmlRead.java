package me.project.feeddownload.bu;


import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileWriter;
import com.alibaba.excel.EasyExcel;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.Dom4JDriver;
import me.project.feeddownload.bu.typlog.ItemBean;
import me.project.feeddownload.bu.typlog.OutExcelBean;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 喷泉公园
 */
public class PqgyFeedXmlRead {

    /**
     * item
     */
    private static ItemBean parseItemXml(String filePath) {
        File xmlFile = FileUtil.file(filePath);
        XStream xstream = new XStream(new Dom4JDriver());
        xstream.processAnnotations(ItemBean.class);
        XStream.setupDefaultSecurity(xstream);
        xstream.allowTypes(new Class[]{ItemBean.class, ItemBean.Item.class});
        ItemBean bo = (ItemBean) xstream.fromXML(xmlFile);
        System.out.println(bo.getItemList().size());
        return bo;
    }

    /**
     * 写入 txt
     *
     * @param filePath
     */
    private static void writeToTxt(String filePath, String outFilePath) {
        ItemBean itemBean = PqgyFeedXmlRead.parseItemXml(filePath);
        StringBuilder content = new StringBuilder();
        itemBean.getItemList().forEach(item -> {
            String tile = item.getTitle().trim();
            String url = item.getEnclosure().getUrl().trim();
            content.append(tile).append("\n").append(url).append("\n\n");
        });
        //写入文件
        FileWriter writer = new FileWriter(outFilePath);
        writer.write(content.toString());
        System.out.println("写入完成");
    }

    /**
     * 写入 excel
     *
     * @param filePath
     * @param outFilePath
     */
    private static void writeToExcel(String filePath, String outFilePath) {
        ItemBean itemBean = PqgyFeedXmlRead.parseItemXml(filePath);
        List<OutExcelBean> content = new ArrayList<>();
        itemBean.getItemList().forEach(item -> {
            OutExcelBean bean = OutExcelBean.builder()
                    .title(item.getTitle().trim())
                    .description(item.getDescription().trim())
                    .summary(item.getSummary().trim())
                    .duration(item.getDuration().trim())
                    .pubDate(item.getPubDate())
                    .url(item.getEnclosure().getUrl())
                    .build();
            content.add(bean);
        });
        //写入文件
        System.out.println("写入文件，" + outFilePath);
        EasyExcel.write(outFilePath, OutExcelBean.class).sheet("文件").doWrite(content);
        System.out.println("写入完成");
    }

    private String parseUrl(String url) {
        String[] str = url.split("/");
        String fileName = str[str.length - 1];
        String urlPrefix = "https://v.typlog.com/penquan/";
        return urlPrefix + fileName;
    }

    public static void main(String[] args) {
        String projectPath = System.getProperty("user.dir");
        String filePath = projectPath + File.separator + "feed-download" + File.separator + "data" + File.separator + "pqgy" + File.separator;
        String fileName = "feed-200928.xml";
        System.out.println("读取文件：" + filePath + fileName);

        String outFileName = "feed-200928_" + System.currentTimeMillis() + ".xlsx";

//        writeToTxt(filePath + fileName);
        writeToExcel(filePath + fileName, filePath + outFileName);


    }
}
