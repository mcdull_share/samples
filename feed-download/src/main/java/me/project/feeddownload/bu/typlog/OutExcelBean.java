package me.project.feeddownload.bu.typlog;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.HeadFontStyle;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
@HeadFontStyle(fontHeightInPoints = 10)
public class OutExcelBean {

    @ExcelProperty("标题")
    private String title;

    @ExcelProperty("时长")
    private String duration;

    @ExcelProperty("url")
    private String url;

    @ExcelProperty("description")
    private String description;

    @ExcelProperty("summary")
    private String summary;

    @ExcelProperty("发布日期")
    private String pubDate;

}
