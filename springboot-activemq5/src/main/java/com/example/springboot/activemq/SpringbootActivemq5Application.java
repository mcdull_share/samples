package com.example.springboot.activemq;

import com.example.springboot.activemq.bean.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsOperations;

@Slf4j
@EnableJms
@SpringBootApplication
public class SpringbootActivemq5Application implements CommandLineRunner {


    @Autowired
    private JmsOperations jmsTemplate;

    public static void main(String[] args) {
        SpringApplication.run(SpringbootActivemq5Application.class, args);
    }

    /**
     * 发送消息，destination是发送到的队列，message是待发送的消息
     */
    @Override
    public void run(String... args) throws Exception {
        for (int i = 0; i < 10; ++i) {
            jmsTemplate.convertAndSend("test", "message");
            jmsTemplate.convertAndSend("user", User.builder().age("10").name("mj").build());
        }
    }

    // 消费者1
    @JmsListener(destination = "test")
    public void receive1(String message) {
        log.info("One Receive: " + message);
    }
    // 消费者2
    @JmsListener(destination = "user")
    public void receive2(User message) {
        log.info("Two Receive: {}", message);
    }
}
