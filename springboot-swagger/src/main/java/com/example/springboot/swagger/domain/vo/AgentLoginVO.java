package com.example.springboot.swagger.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author jonas
 * @date 2019-08-27.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "代理商登陆返回")
public class AgentLoginVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "代理商编号", name = "agentNo", required = true)
    private String agentNo;
    @ApiModelProperty(value = "token", name = "token", required = true)
    private String token;
    @ApiModelProperty(value = "用户姓名", name = "name", required = true)
    private String name;
    @ApiModelProperty(value = "手机号", name = "phone", required = true)
    private String phone;
}
