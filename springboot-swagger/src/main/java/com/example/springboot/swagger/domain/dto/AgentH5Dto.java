package com.example.springboot.swagger.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Description: 获取代理商专属产品参数对象
 * @author gangwei.chen
 * @date Jul 3, 2019 6:11:01 PM
 * @version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "获取代理商专属产品参数对象VO")
public class AgentH5Dto implements Serializable {

	private static final long serialVersionUID = 1L;
    
	@ApiModelProperty(value="代理商编号",name="agentNo")
    private String agentNo;

//    @ApiModelProperty(value="产品ID",name="productId")
//    private String productId;
}
