package com.example.springboot.swagger.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @param <T>
 * @author gangwei.chen
 * @version V1.0
 * @Description: 分页
 * @date Jun 27, 2019 5:17:04 PM
 */
@ApiModel("分页对象")
public class PageModel<T> implements Serializable {

    private static final long serialVersionUID = 4217820035275155157L;
    @ApiModelProperty("第几页")
    private int pageNo;// 第几页
    @ApiModelProperty("每页条数")
    private int pageSize;// 页宽
    @ApiModelProperty("总页数")
    private int pageCount;// 一共的页数
    @ApiModelProperty("总条数")
    private int totalCount;// 记录条数

    @ApiModelProperty("对象实体")
    private List<T> entityList;// 查询出来的对象实体

    public PageModel() {
        this.pageSize = 10;
        this.pageNo = 1;
    }

    public PageModel(int pageNo, int pageSize) {
        super();
        this.pageNo = pageNo < 1 ? 1 : pageNo;
        this.pageSize = pageSize < 1 ? 10 : pageSize;
    }

    public int getPageNo() {
        return this.pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageCount() {

        return this.pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getTotalCount() {
        return this.totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.pageCount = (int) Math.ceil((double) totalCount / this.pageSize);
        this.totalCount = totalCount;
    }


    public List<T> getEntityList() {
        return this.entityList;
    }

    public void setEntityList(List<T> entityList) {
        this.entityList = entityList;
    }

    @Override
    public String toString() {
        return "Page{" + "pageNo=" + this.pageNo + ", pageSize=" + this.pageSize + ", pageCount="
                + this.pageCount + ", totalCount=" + this.totalCount + ", entityList="
                + this.entityList + '}';
    }
}
