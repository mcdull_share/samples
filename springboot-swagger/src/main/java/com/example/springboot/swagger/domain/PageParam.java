package com.example.springboot.swagger.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

/**
 * @Description: 分页查询参数
 * @author gangwei.chen
 * @date 2019年6月27日下午5:28:48
 * @version V1.0
 */

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "分页查询对象DTO")
public class PageParam<T, V> implements Serializable {

    private static final long serialVersionUID = 256927723141005105L;

    @ApiModelProperty("分页对象实体")
    private PageModel<T> page;

    @ApiModelProperty("参数对象实体")
    private V param;
}
