package com.example.springboot.swagger.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author jonas
 * @date 2019-08-27.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "代理商登陆")
public class AgentLoginDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "登陆名（手机号）", name = "phone", required = true)
    private String phone;
    @ApiModelProperty(value = "密码", name = "passwd", required = true)
    private String passwd;
    @ApiModelProperty(value = "验证码", name = "imgCode", required = false)
    private String imgCode;

}
