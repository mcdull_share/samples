package com.example.springboot.swagger.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;

/**
 * @Description: 银行分类返回对象
 * @author gangwei.chen
 * @date 2019-09-17
 * @version V1.3.0
 */
@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "银行分类返回对象VO")
public class BankVO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value="银行代码",name="bankCode")
    private String bankCode;

	@ApiModelProperty(value="银行名称",name="bankName")
    private String bankName;

	@ApiModelProperty(value="银行logo地址",name="bankLogo")
    private String bankLogo;

	@ApiModelProperty(value="银行简称",name="bankShortName")
    private String bankShortName;

	@ApiModelProperty(value="英文缩写",name="enName")
    private String enName;
}
