package com.example.springboot.swagger.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/***
 * @Description: API接口请求参数
 * @author gangwei.chen
 * @date 2019年6月24日上午10:35:36
 * @version V1.0
 * 	// 按字母a-z参数排序，一级参数全部参与签名，报文为json格式
 * 	// 签名算法：RSA/HMacSHA256。
 * 	// 签名密钥：双方约定key
 * 	// 签名源串：json报文 + key
 * 	// 签名值： HMacSHA256{签名源串}
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "请求实体")
public class RequestBean<T> implements Serializable {

    private static final long serialVersionUID = -8163427155906987911L;


    @ApiModelProperty(value = "系统分配给应用的应用编号", name = "appId", required = true)
    private String appId;

    @ApiModelProperty(value = "请求的时间戳，接入系统的时间误差不能超过10分钟，格式为：yyyy-MM-dd HH:mm:ss，如：2019-07-17 12:58:15", name = "timestamp", required = true)
    private String timestamp;

    @ApiModelProperty(value = "请求流水号", name = "reqNo", required = true)
    private String reqNo;

    @ApiModelProperty(value = "接口版本 默认1.0", name = "v", required = true)
    private String v;

    @ApiModelProperty(value = "服务请求的JSON对象", name = "body", required = true)
    private T body;

    @ApiModelProperty(value = "签名类型 1:RSA 2:HMacSHA256", name = "signType", required = true)
    private String signType;

    @ApiModelProperty(value = "签名后字符串，通过签名机制，防止应用的请求参数被非法篡改，业务系统必须保证该值不被泄露", name = "signValue", required = true)
    private String signValue;


}
