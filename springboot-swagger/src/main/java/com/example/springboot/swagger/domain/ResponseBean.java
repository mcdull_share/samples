package com.example.springboot.swagger.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ApiModel(description = "返回结果")
public class ResponseBean<T> implements Serializable, Cloneable {

    private static final long serialVersionUID = -7013365184189806099L;

    @ApiModelProperty(value = "响应码 0000:成功 其他:失败", name = "code")
    private String code = "0000";

    @ApiModelProperty(value = "响应信息", name = "msg")
    private String msg = "执行成功";

    @ApiModelProperty(value = "返回内容，服务调用成功后的返回结果，是一个嵌套的String类型的JSON对象", name = "body")
    private T body;

    @ApiModelProperty(value = "请求流水号", name = "reqNo")
    private String reqNo;

    @ApiModelProperty(value = "签名类型 1:RSA 2:HMacSHA256", name = "signType")
    private String signType;

    @ApiModelProperty(value = "签名字符串", name = "signValue")
    private String signValue;


}
