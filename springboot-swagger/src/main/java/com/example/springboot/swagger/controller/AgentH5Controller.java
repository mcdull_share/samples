package com.example.springboot.swagger.controller;

import com.example.springboot.swagger.domain.RequestBean;
import com.example.springboot.swagger.domain.ResponseBean;
import com.example.springboot.swagger.domain.dto.AgentH5Dto;
import com.example.springboot.swagger.domain.dto.AgentLoginDto;
import com.example.springboot.swagger.domain.vo.AgentLoginVO;
import com.example.springboot.swagger.domain.vo.BankVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author gangwei.chen
 * @version V1.0
 * @Description: 代理商H5平台查询接口
 * @date Aug 14, 20195:31:25 PM
 */
@RestController
@Api(value = "代理商H5平台查询接口", tags = {"代理商H5平台接口列表"})
@RequestMapping("/api/agent")
@Slf4j
public class AgentH5Controller {


    @ApiOperation(value = "代理商登陆", notes = "返回代理商编号")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseBean<AgentLoginVO> agentLogin(@RequestBody RequestBean<AgentLoginDto> dto) {
        return null;
    }

    @ApiOperation(value = "代理商已结算金额", notes = "返回代理商已结算金额")
    @RequestMapping(value = "/getAgentSettledAmt", method = RequestMethod.POST)
    public ResponseBean<String> getAgentSettledAmt(@RequestBody RequestBean<AgentH5Dto> dto) {
        return null;
    }

    @ApiOperation(value = "银行分类查询", notes = "返回银行分类信息")
    @RequestMapping(value = "/getBankList", method = RequestMethod.POST)
    public ResponseBean<List<BankVO>> getBankList(@RequestBody RequestBean<String> dto) {
        // 请求流水号
        String reqNo = dto.getReqNo();
        return null;
    }
}
