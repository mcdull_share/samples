package com.example.util;


import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;

public class IdUtils extends IdUtil {

    public static void main(String[] args) {
        //生成的UUID是带-的字符串，类似于：a5c8a5e8-df2b-4706-bea4-08d0939410e3
        String uuid = IdUtil.randomUUID();

        //生成的是不带-的字符串，类似于：b17f24ff026d40949c85a24f4f375d42
        String simpleUUID = IdUtil.simpleUUID();

        System.out.println(uuid);
        System.out.println(simpleUUID);

        Snowflake snowflake = IdUtil.createSnowflake(1,1);
        System.out.println(snowflake.nextIdStr());

        System.out.println(IdUtil.objectId());
        System.out.println(IdUtil.objectId());
        System.out.println(IdUtil.objectId());
    }
}
