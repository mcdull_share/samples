package com.example.util;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;

public class DateUtils extends DateUtil {


    /**
     * yyyy-MM-dd HH:mm:ss
     * yyyy/MM/dd HH:mm:ss
     * yyyy.MM.dd HH:mm:ss
     * yyyy年MM月dd日 HH时mm分ss秒
     * yyyy-MM-dd
     * yyyy/MM/dd
     * yyyy.MM.dd
     * HH:mm:ss
     * HH时mm分ss秒
     * yyyy-MM-dd HH:mm
     * yyyy-MM-dd HH:mm:ss.SSS
     * yyyyMMddHHmmss
     * yyyyMMddHHmmssSSS
     * yyyyMMdd
     * EEE, dd MMM yyyy HH:mm:ss z
     * EEE MMM dd HH:mm:ss zzz yyyy
     * yyyy-MM-dd'T'HH:mm:ss'Z'
     * yyyy-MM-dd'T'HH:mm:ss.SSS'Z'
     * yyyy-MM-dd'T'HH:mm:ssZ
     * yyyy-MM-dd'T'HH:mm:ss.SSSZ
     *
     * return 2019-06-01
     *
     * @param str
     * @return
     */
    public static String formatString(String str) {
        return format(parse(str), DatePattern.NORM_DATE_PATTERN);

    }

    public static void main(String[] args) {
        System.out.println(DateUtils.formatString("20190909"));
    }

}
