package me.project.ch3.Singleton;

/**
 * @author mbryce
 * @date 2018/6/9
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Start ...");
        Singleton singleton = Singleton.getInstance();
        Singleton singleton1 = Singleton.getInstance();
        if (singleton == singleton1){
            System.out.println("同一个实例");
        } else {
            System.out.println("不同的实例");
        }
        System.out.println("End ...");
    }
}
