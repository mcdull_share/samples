package me.project.ch3.prototype;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mbryce
 * @date 2018/6/9
 */
public class Manager {
    private Map showCase = new HashMap();

    public void register(String name, Product product) {
        this.showCase.put(name,product);
    }
    public Product create(String protoName) {
        Product p = (Product) showCase.get(protoName);
        return p.createClone();
    }
}
