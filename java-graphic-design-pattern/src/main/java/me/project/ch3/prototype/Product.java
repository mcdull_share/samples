package me.project.ch3.prototype;

/**
 * @author mbryce
 * @date 2018/6/9
 */
public interface Product extends Cloneable {
    public abstract void use(String s);
    public abstract Product createClone();

}
