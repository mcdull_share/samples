package me.project.ch3.prototype;

/**
 * @author mbryce
 * @date 2018/6/9
 */
public class Main {
    public static void main(String[] args) {
        Manager manager = new Manager();
        MessageBox mbox = new MessageBox('*');
        MessageBox sbox = new MessageBox('/');
        manager.register("warning box",mbox);
        manager.register("slash box",sbox);

        Product p1 = manager.create("warning box");
        p1.use("hello world.");
        Product p2 = manager.create("slash box");
        p2.use("hello world.");
    }

}
