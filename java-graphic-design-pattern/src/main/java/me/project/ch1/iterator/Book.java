package me.project.ch1.iterator;

/**
 * @author Mcdull
 * @date 2018-6-6
 */
public class Book {
    private String name;
    public Book(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
}
