package me.project.ch1.iterator;

/**
 * 集合API，创建迭代规则
 * @author Mcdull
 * @date 2018-6-6
 */
public interface Aggregate {
    public abstract Iterator iterator();
}
