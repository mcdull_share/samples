package me.project.ch1.iterator;

/**
 * 迭代器API
 * @author Mcdull
 * @date 2018-6-6
 */
public interface Iterator {
    public abstract boolean hasNext();
    public abstract Object next();
}
