package me.project.ch1.iterator;

import java.util.ArrayList;

/**
 * 具体的集合
 * @author Mcdull
 * @date 2018-6-6
 */
public class BookShelf implements Aggregate{
   // private Book[] books;
    private ArrayList<Book> books;
    private int last = 0;

    public BookShelf(int maxSize) {
        //this.books = new Book[maxSize];
        this.books = new ArrayList<Book>(maxSize);
    }

    public Book getBookAt(int index){
        //return books[index];
        return books.get(index);
    }

    public void appendBook(Book book){
        //this.books[last] = book;
        this.books.add(book);
        last ++;
    }

    public int getLength(){
        //return this.last;
        return this.books.size();
    }

    public Iterator iterator() {
        return new BookShelfIterator(this);
    }
}
