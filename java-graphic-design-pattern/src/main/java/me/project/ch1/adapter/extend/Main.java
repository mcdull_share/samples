package me.project.ch1.adapter.extend;

/**
 * @author Mcdull
 * @date 2018-6-7
 */
public class Main {
    public static void main(String[] args) {
        Print print = new PrintBanner("hello");
        print.printStrong();
        print.printWeak();
    }
}
