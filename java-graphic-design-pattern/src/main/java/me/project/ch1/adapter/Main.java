package me.project.ch1.adapter;

/**
 * @author Mcdull
 * @date 2018-6-8
 */
public class Main {
    public static void main(String[] args) {
        Print print = new PrintBanner("hello");
        print.printStrong();
        print.printWeak();
    }
}
