package me.project.ch1.adapter;

/**
 * @author Mcdull
 * @date 2018-6-7
 */
public abstract class Print {
    public abstract void printWeak();
    public abstract void printStrong();
}
