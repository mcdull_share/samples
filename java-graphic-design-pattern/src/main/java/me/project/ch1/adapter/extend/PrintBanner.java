package me.project.ch1.adapter.extend;

/**
 * @author Mcdull
 * @date 2018-6-7
 */
public class PrintBanner extends Banner implements Print {
    public PrintBanner(String string) {
        super(string);
    }

    public void printWeak() {
        showWithParen();
    }

    public void printStrong() {
        showWithAster();
    }
}
