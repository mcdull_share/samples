package me.project.ch1.adapter;

import me.project.graphic_design_pattern.ch1.adapter.extend.Banner;

/**
 * @author Mcdull
 * @date 2018-6-7
 */
public class PrintBanner extends Print {
    private Banner banner;
    public PrintBanner(String string) {
        this.banner = new Banner(string);
    }

    public void printWeak() {
        banner.showWithParen();
    }

    public void printStrong() {
        banner.showWithAster();
    }
}
