package me.project.factory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mcdull
 * @date 2018-6-8
 */
public class IDCardFactory extends Factory {
    private List<String> owners = new ArrayList<String>();
    protected Product createProduct(String owner) {
        return new IDCard(owner);
    }

    protected void registerProduct(Product product) {
        owners.add(((IDCard)product).getOwner());
    }
    public List<String> getOwners(){
        return owners;
    }
}
