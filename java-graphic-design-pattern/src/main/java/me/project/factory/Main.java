package me.project.factory;

/**
 * @author Mcdull
 * @date 2018-6-8
 */
public class Main {
    public static void main(String[] args) {
        Factory factory = new IDCardFactory();
        Product card1 = factory.create("card1");
        Product card2 = factory.create("card2");
        Product card3 = factory.create("card3");
        Product card4 = factory.create("card4");
        card1.use();
        card2.use();
        card3.use();
        card4.use();
    }
}
