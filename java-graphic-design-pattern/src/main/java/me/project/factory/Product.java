package me.project.factory;

/**
 * @author Mcdull
 * @date 2018-6-8
 */
public abstract class Product {
    public abstract void use();
}
