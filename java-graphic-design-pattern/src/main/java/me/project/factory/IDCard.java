package me.project.factory;

/**
 * @author Mcdull
 * @date 2018-6-8
 */
public class IDCard extends Product {
    private String owner;
    IDCard(String owner) {
        System.out.println("制作 " + owner +" 的ID卡。");
        this.owner = owner;
    }
    public void use() {
        System.out.println("使用 " + owner +" 的ID卡。");
    }
    public String getOwner(){
        return this.owner;
    }
}
