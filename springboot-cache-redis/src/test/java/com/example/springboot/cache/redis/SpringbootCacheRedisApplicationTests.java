package com.example.springboot.cache.redis;

import com.example.springboot.cache.redis.service.RedisBaseService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootCacheRedisApplicationTests {

    @Autowired
    private RedisBaseService redisBaseService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void contextLoads() {
        redisBaseService.set("redis_test","111");
        redisTemplate.opsForValue().set("redis_test_2",User.builder().name("mm").age(12).build());

        User user = (User) redisTemplate.opsForValue().get("redis_test_2");
        String a = String.valueOf(redisTemplate.opsForValue().get("redis_test"));
        log.info("{}",user);
        log.info("{}", a);

    }

}
