package com.example.springboot.cache.redis.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

/**
 * Redis
 */
@Slf4j
@Component
public class RedisService {
    /**
     * 默认一天
     */
    private final Long DEFAULT_TIME = 24 * 60 * 60L;


    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    private String keySetting(String key) {
        return "BASE" + key;
    }

    /**
     * 删除缓存<br>
     * 根据key精确匹配删除
     */
    public void del(String key) {
        if (StringUtils.isEmpty(key)) {
            throw new RuntimeException("key is not exist");
        }
        key = keySetting(key);
        try {
            redisTemplate.delete(key);
            log.info("删除缓存成功");
        } catch (Exception e) {
            log.info("系统处理异常", e);
        }
    }

    /**
     * 取得缓存（字符串类型）
     */
    public String getStr(String key) {
        if (StringUtils.isEmpty(key)) {
            throw new RuntimeException("key is not exist");
        }
        key = keySetting(key);
        String result = null;
        try {
            result = String.valueOf(redisTemplate.opsForValue().get(key));
        } catch (Exception e) {
            log.info("系统处理异常", e);
        }
        return result;
    }

    /**
     * 获取缓存<br>
     * 注：该方法暂不支持Character数据类型
     */
    public <T> T get(String key) {
        if (StringUtils.isEmpty(key)) {
            throw new RuntimeException("key is not exist");
        }
        key = keySetting(key);
        T t = null;
        try {
            t = (T) redisTemplate.boundValueOps(key).get();
        } catch (Exception e) {
            log.info("系统处理异常", e);
        }
        return t;
    }


    /**
     * 将value对象写入缓存
     *
     * @param expireTime 失效时间(秒)
     */
    public void set(String key, Object value, Long expireTime) {
        if (StringUtils.isEmpty(key) ) {
            throw new RuntimeException("key is not exist");
        }
        key = keySetting(key);
        try {
            redisTemplate.opsForValue().set(key, value);
            if (expireTime != null && expireTime > 0) {
                redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
            } else {
                redisTemplate.expire(key, DEFAULT_TIME, TimeUnit.SECONDS);
            }
        } catch (Exception e) {
            log.info("redis 缓存异常 {}", e.getMessage());
        }
    }

    public Long increment(String key) {
        if (StringUtils.isEmpty(key)) {
            throw new RuntimeException("key is not exist");
        }
        Long seq = 0L;
        key = keySetting(key);
        try {
            seq = redisTemplate.opsForValue().increment(key, 1);
            return seq;
        } catch (Exception e) {
            log.info("redis 缓存异常 {}", e.getMessage());
        }
        return null;
    }


}
