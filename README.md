# samples

| 项目                         | 描述                         |
| :--------------------------- | ---------------------------- |
| example                      | 书籍源代码                   |
| spring-amqp                  | rabbitmq                     |
| spring-amqp-samples          | rabbitmq                     |
| java-graphic-design-pattern  | 图解设计模式原书比较，源代码 |
| springboot-filter-cors       | 过滤器，跨域配置             |
| springboot-mybatis-generator |                              |
| springboot-swagger           | 集成io.springfox             |

