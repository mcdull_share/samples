package me.project.dl_jvm.ch7;

/**
 * @author Mcdull
 */
public class SubClass extends SuperClass{
    static {
        System.out.println("SubClass init!");
    }
}