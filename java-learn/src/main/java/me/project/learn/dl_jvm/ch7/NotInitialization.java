package me.project.dl_jvm.ch7;

/**
 * @author Mcdull
 * @date 2018-5-23
 */
public class NotInitialization {
    public static void main(String[] args) {
        /*
         * 1.子类引用父类静态变量，不会初始化子类
         */
        //System.out.println(SubClass.value);

        /*
         * 2.通过数组定义引用类，不会初始化此类
         */
        //SuperClass[] sc = new SuperClass[10];

        /*
         * 3. 常量存入调用类的常量池
         */
        System.out.println(ConstClass.HELLO_WORLD);
    }
}
