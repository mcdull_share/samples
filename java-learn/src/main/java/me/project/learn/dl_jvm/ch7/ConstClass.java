package me.project.dl_jvm.ch7;

/**
 * @author Mcdull
 * @date 2018-5-23
 */
public class ConstClass {
    static {
        System.out.println("ConstClass init!");
    }
     public static String HELLO_WORLD = "hello world";
}
