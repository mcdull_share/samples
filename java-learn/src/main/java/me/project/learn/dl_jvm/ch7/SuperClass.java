package me.project.dl_jvm.ch7;

/**
 * @author Mcdull
 */
public class SuperClass{
    static{
        System.out.println("SuperClass init!");
    }
    public static int value = 123;
}