package me.project.learn.rsa;

import cn.hutool.crypto.asymmetric.AsymmetricAlgorithm;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

/**
 * Rsa 测试
 */
public class RSATest {
    private String rsaPubKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC/5Rc2tqQPO2FegBQjD/ChwAvURdHPuSm/6i1g8S0K88cQE/v3oWH0VBt8tHe9PrO76erxnmkmPP74dOtqFEelnwnueo+5o7ZEAEJ5hoVymv7lKCVp9jvhTSd5pWhBiG+59DO34M5E+k9n5vnzsPMtnckrhdcP+n2bjsbNj8TZ8QIDAQAB";
    private String rsaPriKey = "MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAL/lFza2pA87YV6AFCMP8KHAC9RF0c+5Kb/qLWDxLQrzxxAT+/ehYfRUG3y0d70+s7vp6vGeaSY8/vh062oUR6WfCe56j7mjtkQAQnmGhXKa/uUoJWn2O+FNJ3mlaEGIb7n0M7fgzkT6T2fm+fOw8y2dySuF1w/6fZuOxs2PxNnxAgMBAAECgYAv/lUS+xUHNjvBDdl7d3Ygjk6nFZtHD7FwbVpSH5+7WIMV81mWcod0O0DwrJYx/6emPPfk3FDCHolKjLuAOXg9deE61eYXhArk9OPlAZFFDS8nZFGJXcO07DXw4TD0UKF+n5AXlyY08pkKtwlWsAQmo7P6pEtx9KMpEO/x4FTXPQJBAO6QyG8vTbXBVHvXhPRtk6gyVLHnNSKEPimwn3cNIwykEE4jB748nbFGGso9x7yrZ2MXPAZpxlGl4AEnlySRpE8CQQDN6yq2WgL45hkkugRWhUiBcCREL/elNG69howaXfglpdjErnpDftzz8qZWqZ8WRLPxR33CxZMAfa6Wpi5m+M2/AkEAyYqbKgMmfXRUsUMD2ZNw+Nl8X8daRky+T24dafA5Ogaol7pUYkzHU+pL26uRwDx13WrVlP1rQ7MEqTUPJR+9VQJBAMMIEZRU/Kuxg+79IE6yPlIaXL8Yj32xg5GOdfQjPHyr7/uZJcP3zF5hXJqIXV1CBQxpoftwFiNI5qtZCmoNDRECQQCoFMkF+ZN7gOXTUcK2GvzHVyFG9jrH8G8Ot3zv/hM0GbkppJTQyKj1897Myfk0TtWAfhaxp0gUxjBlBb+3utMZ";


    @Test
    public void encryptAndDecrypt2() throws Exception {
        String content = "21234235678976543213456786543245678976654322456786543223456786543245678654324567865432425676543214"
                + "3456744322456766654332huyytdcddfvgbhjyu656t4rdfbhhgfvbvnhjyhtgfvghhtrgefc nghhtrgefvbfhtrg"
                + "ghrgfvfbgnhtjyyhtgrefvdbgnhtthgtrfvedbghnjythrgfdvfbgnhyjuhtrgfvdfbgnhjyythrgvdfbgnhjythrgvdfbgh"
                + "ghrgfvfbgnhtjyyhtgrefvdbgnhtthgtrfvedbghnjythrgfdvfbgnhyjuhtrgfvdfbgnhjyythrgvdfbgnhjythrgvdfbgh";

        //签名，请求参数
        String enStr = rsaEncryptByPubKey(content, rsaPubKey);
        String contentStr = rsaDecryptByPriKey(enStr, rsaPriKey);
        System.out.println("contentStr = " + contentStr);
    }


    /**
     * rsa 公钥加密
     *
     * @return
     */
    public static String rsaEncryptByPubKey(String content, String pubKey) {
        RSA rsa = new RSA(AsymmetricAlgorithm.RSA.getValue(), null, pubKey);
//        byte[] encrypt = rsa.encrypt(StrUtil.bytes(content, CharsetUtil.CHARSET_UTF_8), KeyType.PublicKey);
////        return new String(encrypt, StandardCharsets.UTF_8);
        return rsa.encryptBcd(content, KeyType.PublicKey);
    }

    /**
     * 私钥解密
     *
     * @param content
     * @param privateKey
     * @return
     */
    public static String rsaDecryptByPriKey(String content, String privateKey) {
        RSA rsa = new RSA(AsymmetricAlgorithm.RSA.getValue(), privateKey, null);
//        byte[] encrypt = rsa.decrypt(StrUtil.bytes(content, CharsetUtil.CHARSET_UTF_8), KeyType.PrivateKey);
//        return new String(encrypt, StandardCharsets.UTF_8);
        return new String(rsa.decryptFromBcd(content, KeyType.PublicKey), StandardCharsets.UTF_8);

    }
}
