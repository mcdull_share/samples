package me.project.learn.xml;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.XmlUtil;
import org.w3c.dom.Document;

import javax.xml.xpath.XPathConstants;
import java.io.File;

public class XmlRead {

    public static void main(String[] args) {
        //当前项目路径
        String projectPath = System.getProperty("user.dir");
        String filePath = projectPath + File.separator + "java-learn" + File.separator + "data" + File.separator;
        String fileName = "feed.xml";
        System.out.println("读取文件：" + filePath + fileName);
        File xmlFile = FileUtil.file(filePath + fileName);
        Document docResult = XmlUtil.readXML(xmlFile);
        XmlUtil.getByXPath("//rss/channel/item", docResult, XPathConstants.STRING);

    }
}
