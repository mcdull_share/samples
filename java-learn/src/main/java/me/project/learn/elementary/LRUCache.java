package me.project.elementary;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author majian
 * @date 2019-2-15 0015
 */
public class LRUCache<K,V> extends LinkedHashMap {

    private int CACHE_SIZE;

    public LRUCache(int cacheSize){
        super((int) (Math.ceil(cacheSize/0.75) + 1), 0.75f, true);
        CACHE_SIZE = cacheSize;
    }


    @Override
    protected boolean removeEldestEntry(Map.Entry eldest) {
        return size() > CACHE_SIZE;
    }


    public static void main(String[] args) {
        LRUCache<String, String> lruCache = new LRUCache<>(3);
        lruCache.put("key1", "1");
        lruCache.put("key2", "2");
        lruCache.put("key3", "3");
        System.out.println(lruCache.toString());
        lruCache.put("key4", "4");
        System.out.println(lruCache.toString());
        lruCache.get("key3");
        System.out.println(lruCache.toString());
        lruCache.remove("key4");
        System.out.println(lruCache.toString());
        lruCache.put("key5", "5");
        System.out.println(lruCache.toString());

    }
}
