package me.project.elementary.generics;

import java.util.ArrayList;

/**
 * 泛型，类型擦除
 * @author majian
 * @date 2019-3-4 0004
 */
public class ErasedTypeEquivalence {
    public static void main(String[] args) {
        Class c1 = new ArrayList<String>().getClass();
        Class c2 = new ArrayList<Integer>().getClass();
        System.out.println(c1 == c2); // true
    }
}