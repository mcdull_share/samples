package me.project.elementary;

/**
 * @author Mcdull
 * @date 2018-6-14
 */
public class EnumConstant {
    /**
     * 省市县
     */
    public enum area {
        PROVINCE("0", "省"),
        CITY("1", "市"),
        COUNTY("2", "县");

        area(String value, String name) {
            this.value = value;
            this.name = name;
        }

        private final String value;
        private final String name;

        public String getValue() {
            return value;
        }

        public String getName() {
            return name;
        }
    }
    
    /**
     * 地区标准
     */
    public enum areaStandardType {
        UNION_PAY("UnionPay_Area_MAP", "银联标准"),
        GB("GB_Area_MAP", "国标");

        areaStandardType(String value, String name) {
            this.value = value;
            this.name = name;
        }

        private final String value;
        private final String name;

        public String getValue() {
            return value;
        }

        public String getName() {
            return name;
        }

    }
}
