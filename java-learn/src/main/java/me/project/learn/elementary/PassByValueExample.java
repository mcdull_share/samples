package me.project.elementary;

/**
 * Dog dog 的 dog 是一个指针, 指向对象的地址
 * @author Mcdull
 * @date 2018-7-16
 */
public class PassByValueExample {
    public static void main(String[] args) {
        Dog dog = new Dog("A");
        System.out.println("main:dog.getObjectAddress()" + dog.getObjectAddress()); // Dog@4554617c
        System.out.println(dog.toString());
        func(dog);
        System.out.println("main:dog.getObjectAddress()" + dog.getObjectAddress()); // Dog@4554617c
        System.out.println("main:dog.getName()" + dog.getName());          // A
    }

    private static void func(Dog dog) {
        System.out.println(dog.toString());
        System.out.println("func:dog.getObjectAddress()" + dog.getObjectAddress()); // Dog@4554617c
        dog = new Dog("B");
        System.out.println("func:dog.getObjectAddress()" + dog.getObjectAddress()); // Dog@74a14482
        System.out.println("func:dog.getObjectAddress()" + dog.getName());          // B
    }
}
class Dog {
    String name;

    Dog(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }

    String getObjectAddress() {
        return super.toString();
    }
}
