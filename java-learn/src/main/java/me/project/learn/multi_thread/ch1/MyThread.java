package me.project.multi_thread.ch1;

public class MyThread extends Thread {
	@Override
	public void run() {
		try {
            for (int i = 0; i < 10; i++) {
                int time = (int) (Math.random()*1000);
                Thread.sleep(time);
                System.out.println("run: id=" +Thread.currentThread().getId()+ ",name=" + Thread.currentThread().getName());
            }
		}catch (Exception e){
		    e.printStackTrace();
        }
	}
}
