package me.project.multi_thread.ch1;

/**
 * @author Mcdull
 * @date 2018-6-4
 */
public class Test {
    public static void main(String[] args) throws InterruptedException {
        MyThread myThread = new MyThread();
        myThread.start();
        for (int i = 0; i < 10; i++) {
            int time = (int) (Math.random()*1000);
            Thread.sleep(time);
            System.out.println("main: id=" +Thread.currentThread().getId()+ ",name=" + Thread.currentThread().getName());
        }
    }
}
