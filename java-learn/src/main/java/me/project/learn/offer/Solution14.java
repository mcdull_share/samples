package me.project.offer;

/**
 * 把一根绳子剪成多段，并且使得每段的长度乘积最大。
 * For example,
 * given n = 2, return 1 (2 = 1 + 1);
 * given n = 10, return 36 (10 = 3 + 3 + 4).
 * <br/>
 * <p>
 * note:
 * 动态规划：f(n) = f(i) * f(n-i) 保持最大，循环至 i 接近 n-i；
 *
 * @author Mcdull
 * @date 2018-7-4
 */
public class Solution14 {
    public int integerBreak(int n) {
        int[] dp = null;
        if (n < 4) {
            dp = new int[4];
        } else {
            dp = new int[n + 1];
        }

        dp[2] = dp[1] = 1;
        dp[3] = 2;
        for (int i = 4; i <= n; i++) {
            for (int j = 1; j <= i / 2; j++) {
                dp[i] = Math.max(dp[i], dp[j] * dp[i - j]);
            }
        }
        return dp[n];
    }
}
