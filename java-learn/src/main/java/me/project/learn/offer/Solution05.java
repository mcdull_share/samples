package me.project.offer;

/**
 * 请实现一个函数，将一个字符串中的空格替换成“%20”。
 * 例如，当字符串为We Are Happy.则经过替换之后的字符串为We%20Are%20Happy。
 *
 * @author Mcdull
 * @date 2018-6-29
 */
public class Solution05 {
    /**
     * AC
     * O(1) + O(N)
     *
     * @param str
     * @return
     */
    public String replaceSpace(StringBuffer str) {
        int len = str.length();
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < len; i++) {
            if (str.charAt(i) != ' ') {
                s.append(str.charAt(i));
            } else {
                s.append("%20");
            }
        }
        return s.toString();
    }

    public static void main(String[] args) {
        String a = "we ";
        System.out.println(a.length());
    }
}
