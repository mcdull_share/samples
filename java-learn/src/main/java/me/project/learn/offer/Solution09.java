package me.project.offer;

import java.util.EmptyStackException;
import java.util.Stack;

/**
 * 用两个栈来实现一个队列，完成队列的 Push 和 Pop 操作。
 * 队列中的元素为 int 类型。
 *
 * Note：
 *      in 入栈；
 *      out 出栈；
 *      pop 时候，将out出栈，
 *           若out为空，则将in反转到out，
 *           若out不空，继续出栈
 *      @author mbryce
 *      @date 2018/7/1
 */
public class Solution09 {
    Stack<Integer> in = new Stack<Integer>();
    Stack<Integer> out = new Stack<Integer>();

    public void push(int node) {
        in.push(node);
    }

    public int pop() throws EmptyStackException {
        if(out.isEmpty()){
            while (!in.isEmpty())
                out.push(in.pop());
        }
        if (out.isEmpty()){
            throw new EmptyStackException();
        }
        return out.pop();
    }
}
