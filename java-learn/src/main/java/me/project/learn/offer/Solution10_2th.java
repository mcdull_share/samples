package me.project.offer;

/**
 * 一只青蛙一次可以跳上 1 级台阶，也可以跳上 2 级。求该青蛙跳上一个 n 级的台阶总共有多少种跳法。
 * <p>
 * 假设，一级台阶，有f(1)种方法，二级有f(2)种，以此类推，n级有f(n)种方法。
 * 可以看出，f(1)=1;f(2)=2。
 * 那么，假设n级台阶，那么第一步就有两种情况，跳一步，跟跳两步。
 * 情况一：跳一步，那么接下去的就是f(n-1)；
 * 情况二：跳两步，那么接下去的就是f(n-2)。
 *
 * 解法同 Solution10_1th
 *
 * @author mbryce
 * @date 2018/7/1
 */
public class Solution10_2th {
    /**
     * m 为一步的总数 ，p 为两步的总数
     * m, p 为整数，有解
     * n = 1 * m + 2 * p
     * p = (n - m) / 2
     * 得出的解，排序，全排。
     * 统计每次全排的和
     *
     * @param n
     * @return
     */
    public int JumpFloor(int n) {
        if (n <= 2)
            return n;
        int result = 0;
        for (int m = 3; m <= n; m++) {
            if ((n - m) % 2 == 0) {
                int p = (n - m) / 2;
                //TODO m p 排序
                //result++;
            }
        }
        return result;
    }

}
