package me.project.offer;

/**
 * 把一个数组最开始的若干个元素搬到数组的末尾，我们称之为数组的旋转。
 * 输入一个非递减排序的数组的一个旋转，输出旋转数组的最小元素。
 * 例如数组 {3, 4, 5, 1, 2} 为 {1, 2, 3, 4, 5} 的一个旋转，该数组的最小值为 1。
 * NOTE：给出的所有元素都大于 0，若数组大小为 0，请返回 0。
 *
 * Note:
 *      TODO：二分法
 * @author Mcdull
 * @date 2018-7-2
 */
public class Solution11 {
    public int minNumberInRotateArray(int[] nums) {

        return 0;
    }
}
