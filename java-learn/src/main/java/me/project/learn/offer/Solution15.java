package me.project.offer;

/**
 * @author Mcdull
 * @date 2018-7-4
 */
public class Solution15 {
    /**
     * AC 22ms
     *
     * @param n
     * @return
     */
    public int NumberOf1(int n) {
        return Integer.bitCount(n);
    }

    /**
     * 因为题目给出的是int类型，java中的int类型用32位表示，
     * 把这个数依次无符号右移（考虑到负数所以选择无符号右移）然后与1作与运算。
     * 如果结果为1则count自增
     * @param n
     * @return
     */
    public int NumberOf1_1(int n) {
        int count = 0;
        for(int i = 0; i < 32; i++){
            if((n >>> i & 1) == 1)
                ++count;
        }
        return count;
    }

    /**
     * 利用&运算的特性,把一个整数减去1，再和原整数做与运算，会把该整数最右边一个1变成0.
     * 那么一个整数的二进制有多少个1，就可以进行多少次这样的操作
     * <p>
     * 举个例子：一个二进制数1100，从右边数起第三位是处于最右边的一个1。减去1后，第三位变成0，它后面的两位0变成了1，而前面的1保持不变，因此得到的结果是1011.我们发现减1的结果是把最右边的一个1开始的所有位都取反了。这个时候如果我们再把原来的整数和减去1之后的结果做与运算，从原来整数最右边一个1那一位开始所有位都会变成0。如1100&1011=1000.也就是说，把一个整数减去1，再和原整数做与运算，会把该整数最右边一个1变成0.那么一个整数的二进制有多少个1，就可以进行多少次这样的操作。
     *
     * @param n
     * @return
     */
    public int NumberOf1_2(int n) {
        int count = 0;
        while (n != 0) {
            count++;
            n = n & (n - 1);
        }
        return count;
    }

    /**
     * 加 1 会死循环，
     *
     * @param n
     * @return
     */
    public int NumberOf1_3(int n) {
        int count = 0;
        while (n != 0) {
            count++;
            n = n & (n + 1);
            System.out.println("count: " + count);
        }
        return count;
    }
    /**
     * @param n
     * @return
     */
    public int NumberOf1_4(int n) {
        int count = 0;
        // TODO n 为全1时候，结束，输出0 的个数
        while (n != 1) {
            count++;
            n = n & (n + 1);
            System.out.println("count: " + count);
        }
        return count;
    }

    public static void main(String[] args) {
        Solution15 s = new Solution15();
        int i = 1101;
        System.out.println(i & 1111);
        //System.out.println("+:"+s.NumberOf1_4(1100));
    }
}
