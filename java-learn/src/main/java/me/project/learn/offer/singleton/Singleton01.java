package me.project.offer.singleton;

/**
 * 静态代码块，启动时初始化一个实例
 * @author Mcdull
 * @date 2018-6-28
 */
public class Singleton01 {
    private static Singleton01 singleton = new Singleton01();

    public static Singleton01 getSingleton(){
        return singleton;
    }

}
