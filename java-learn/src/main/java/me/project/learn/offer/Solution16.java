package me.project.offer;

/**
 * 给定一个 double 类型的浮点数 base 和 int 类型的整数 exponent。
 * 求 base 的 exponent 次方。
 *
 * @author Mcdull
 * @date 2018-7-4
 */
public class Solution16 {


    /**
     * 溢出，
     *
     * @param base
     * @param exponent
     * @return
     */
    public double Power(double base, int exponent) {
        if (base == 0) {
            return 0;
        }
        if (exponent == 1) {
            return base;
        }
        boolean isNegative = false;
        if (exponent < 0) {
            exponent = -exponent;
            isNegative = true;
        }
        double pow = Power(base * base, exponent - 1);
        return isNegative ? 1 / pow : pow;
    }

    /**
     * AC 递归
     * @param base
     * @param exponent
     * @return
     */
    public double Power_2(double base, int exponent) {
        if (base == 0) {
            return 1;
        }
        if (exponent == 1) {
            return base;
        }
        boolean isNegative = false;
        if (exponent < 0) {
            exponent = -exponent;
            isNegative = true;
        }
        double pow = Power(base * base, exponent / 2);
        if (exponent % 2 != 0) {
            pow = pow * base;
        }
        return isNegative ? 1 / pow : pow;
    }
}
