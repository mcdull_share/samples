package me.project.offer.singleton;

/**
 * 内部静态类
 * Singleton02 初始化时候，内部SingletonInstance 不会初始化，实现懒加载
 * @author Mcdull
 * @date 2018-6-28
 */
public class Singleton02 {
    private static class SingletonInstance{
        private final static Singleton02 SINGLETON = new Singleton02();
    }
    public static Singleton02 getInstance(){
        return SingletonInstance.SINGLETON;
    }
}
