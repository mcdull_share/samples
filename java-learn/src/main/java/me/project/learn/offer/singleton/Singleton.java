package me.project.offer.singleton;

/**
 * 双重检查，同步块
 * @author Mcdull
 * @date 2018-6-28
 */
public class Singleton {
    private static volatile Singleton singleton;

    public static Singleton getInstance(){
        if (singleton == null){
            synchronized (Singleton.class){
                if (singleton == null){
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }
}
