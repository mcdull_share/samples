package me.project.learn.juc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 控制多个线程互相等待
 *
 * 通过维护计数器来实现的。线程执行 await() 方法之后计数器会减 1，并进行等待，
 * 直到计数器为 0，所有调用 await() 方法而在等待的线程才能继续执行。
 */
public class CyclicBarrierTest {

    public static void main(String[] args) {
        final int totalThread = 10;
        CyclicBarrier cyclicBarrier = new CyclicBarrier(totalThread);
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < totalThread; i++) {
            executorService.execute(() -> {
                System.out.println("before.." + Thread.currentThread().getName());
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    e.printStackTrace();
                }
                System.out.println("after.." + Thread.currentThread().getName());
            });
        }
        executorService.shutdown();
    }
}
