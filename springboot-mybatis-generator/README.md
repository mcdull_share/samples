# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.1.8.RELEASE/maven-plugin/)

### Guides
The following guides illustrate how to use some features concretely:

* [Accessing data with MySQL](https://spring.io/guides/gs/accessing-data-mysql/)



```$xslt
mybatis.mapper-locations = classpath*:mapper/**/*.xml 
mybatis.type-aliases-package = 类型别名的包名
mybatis.type-handlers-package = TypeHandler扫描包名 
mybatis.configuration.map-underscore-to-camel-case = true
```

```$xslt
@MapperScan 配置扫描位置
@Mapper 定义接口 
映射的定义—— XML 与注解
```


```$xslt
jdbcConnection
javaModelGenerator
sqlMapGenerator
javaClientGenerator (ANNOTATEDMAPPER / XMLMAPPER / MIXEDMAPPER)
table
```