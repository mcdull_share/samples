package com.example.springboot.mybatisplus.generator.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.springboot.mybatisplus.common.entity.QueryRequest;
import com.example.springboot.mybatisplus.common.entity.Column;
import com.example.springboot.mybatisplus.common.entity.Table;

import java.util.List;

/**
 * @author MrBird
 */
public interface IGeneratorService {

    List<String> getDatabases(String databaseType);

    IPage<Table> getTables(String tableName, QueryRequest request, String databaseType, String schemaName);

    List<Column> getColumns(String databaseType, String schemaName, String tableName);
}
