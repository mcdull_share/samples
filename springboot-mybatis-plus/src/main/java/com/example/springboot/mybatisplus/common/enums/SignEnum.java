package com.example.springboot.mybatisplus.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description: 加密类型枚举类
 * @author gangwei.chen
 * @date Jun 26, 2019 5:39:11 PM
 * @version V1.0
 */
@Getter
@AllArgsConstructor
public enum SignEnum {
	RSA("1"), 
	HMacSHA256("2"), 
	TRIPLEDES("3");
	
	private String value ;
}
