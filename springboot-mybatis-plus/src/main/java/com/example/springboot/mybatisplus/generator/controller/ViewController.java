package com.example.springboot.mybatisplus.generator.controller;

import com.example.springboot.mybatisplus.generator.entity.GeneratorConfig;
import com.example.springboot.mybatisplus.common.constant.SysConstant;
import com.example.springboot.mybatisplus.generator.service.IGeneratorConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author MrBird
 */
@Controller("generatorViews")
@RequestMapping(SysConstant.VIEW_PREFIX + "generator")
public class ViewController {

    @Autowired
    private IGeneratorConfigService generatorConfigService;

//    @GetMapping("generator")
////    @RequiresPermissions("generator:view")
//    public String generator() {
//        return FebsUtil.view("generator/generator");
//    }

    @GetMapping("configure")
//    @RequiresPermissions("generator:configure:view")
    public void generatorConfigure(Model model) {
        GeneratorConfig generatorConfig = generatorConfigService.findGeneratorConfig();
        model.addAttribute("config", generatorConfig);
        //return .view("generator/configure");
    }
}
