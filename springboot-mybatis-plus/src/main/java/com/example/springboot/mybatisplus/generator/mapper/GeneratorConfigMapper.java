package com.example.springboot.mybatisplus.generator.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.springboot.mybatisplus.generator.entity.GeneratorConfig;

/**
 * @author MrBird
 */
public interface GeneratorConfigMapper extends BaseMapper<GeneratorConfig> {

}
