package com.example.springboot.mybatisplus.generator.controller;

import com.alibaba.fastjson.JSON;
import com.example.springboot.mybatisplus.generator.entity.GeneratorConfig;
import com.example.springboot.mybatisplus.generator.service.IGeneratorConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author MrBird
 */
@Slf4j
@RestController
@RequestMapping("generatorConfig")
//public class GeneratorConfigController extends BaseController {
public class GeneratorConfigController {

    @Autowired
    private IGeneratorConfigService generatorConfigService;

    @GetMapping
    //@RequiresPermissions("generator:configure:view")
    public String getGeneratorConfig() {
        //return new FebsResponse().success().data(generatorConfigService.findGeneratorConfig());
        return JSON.toJSONString(generatorConfigService.findGeneratorConfig());
    }

    @PostMapping("update")
   // @RequiresPermissions("generator:configure:update")
   // @ControllerEndpoint(operation = "修改GeneratorConfig", exceptionMessage = "修改GeneratorConfig失败")
    public void updateGeneratorConfig(@Valid GeneratorConfig generatorConfig) {

        this.generatorConfigService.updateGeneratorConfig(generatorConfig);
        //return new FebsResponse().success();
    }
}
