create schema examples;

#t_menu
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu`
(
    `MENU_ID`          bigint(20)  NOT NULL AUTO_INCREMENT COMMENT '菜单/按钮ID',
    `PARENT_ID`        bigint(20)  NOT NULL COMMENT '上级菜单ID',
    `MENU_NAME`        varchar(50) NOT NULL COMMENT '菜单/按钮名称',
    `URL`              varchar(50) NULL DEFAULT NULL COMMENT '菜单URL',
    `PERMS`            varchar(20) NULL COMMENT '权限标识',
    `ICON`             varchar(30) NULL DEFAULT NULL COMMENT '图标',
    `TYPE`             varchar(2)  NOT NULL COMMENT '类型 0菜单 1按钮',
    `ORDER_NUM`        bigint(20)  NULL DEFAULT NULL COMMENT '排序',
    `CREATE_TIME`      datetime    NOT NULL COMMENT '创建时间',
    `LAST_UPDATE_TIME` datetime    NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`MENU_ID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '菜单表'
  ROW_FORMAT = Dynamic;

#t_role
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role`
(
    `ROLE_ID`          bigint(20)   NOT NULL AUTO_INCREMENT COMMENT '角色ID',
    `ROLE_NAME`        varchar(100) NOT NULL COMMENT '角色名称',
    `REMARK`           varchar(100) NULL DEFAULT NULL COMMENT '角色描述',
    `CREATE_TIME`      datetime(0)  NOT NULL COMMENT '创建时间',
    `LAST_UPDATE_TIME` datetime(0)  NULL DEFAULT NULL COMMENT '修改时间',
    PRIMARY KEY (`ROLE_ID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '角色表'
  ROW_FORMAT = Dynamic;

#t_role_menu
DROP TABLE IF EXISTS `t_role_menu`;
CREATE TABLE `t_role_menu`
(
    `ROLE_ID` bigint(20) NOT NULL COMMENT '角色ID',
    `MENU_ID` bigint(20) NOT NULL COMMENT '菜单/按钮ID'
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '角色菜单关联表'
  ROW_FORMAT = Dynamic;

#t_user
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user`
(
    `USER_ID`         bigint(20)   NOT NULL AUTO_INCREMENT COMMENT '用户ID',
    `USERNAME`        varchar(50)  NOT NULL COMMENT '用户名',
    `PASSWORD`        varchar(128) NOT NULL COMMENT '密码',
    `DEPT_ID`         bigint(20)   NULL DEFAULT NULL COMMENT '部门ID',
    `EMAIL`           varchar(128) NULL DEFAULT NULL COMMENT '邮箱',
    `MOBILE`          varchar(20)  NULL DEFAULT NULL COMMENT '联系电话',
    `STATUS`          varchar(1)   NOT NULL COMMENT '状态 0锁定 1有效',
    `CREATE_TIME`     datetime     NOT NULL COMMENT '创建时间',
    `MODIFY_TIME`     datetime     NULL DEFAULT NULL COMMENT '修改时间',
    `LAST_LOGIN_TIME` datetime     NULL DEFAULT NULL COMMENT '最近访问时间',
    `SEX`             varchar(1)   NULL DEFAULT NULL COMMENT '性别 0男 1女 2保密',
    `IS_TAB`          varchar(1)   NULL DEFAULT NULL COMMENT '是否开启tab，0关闭 1开启',
    `THEME`           varchar(10)  NULL DEFAULT NULL COMMENT '主题',
    `AVATAR`          varchar(100) NULL DEFAULT NULL COMMENT '头像',
    `DESCRIPTION`     varchar(100) NULL DEFAULT NULL COMMENT '描述',
    PRIMARY KEY (`USER_ID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '用户表'
  ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for t_user_role
-- ----------------------------
DROP TABLE IF EXISTS `t_user_role`;
CREATE TABLE `t_user_role`
(
    `USER_ID` bigint(20) NOT NULL COMMENT '用户ID',
    `ROLE_ID` bigint(20) NOT NULL COMMENT '角色ID'
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '用户角色关联表'
  ROW_FORMAT = Dynamic;


DROP TABLE IF EXISTS `t_generator_config`;
CREATE TABLE `t_generator_config`
(
    `ID`                   INT(11)     NOT NULL COMMENT '主键',
    `AUTHOR`               VARCHAR(20) NOT NULL COMMENT '作者',
    `BASE_PACKAGE`         VARCHAR(50) NOT NULL COMMENT '基础包名',
    `ENTITY_PACKAGE`       VARCHAR(20) NOT NULL COMMENT 'ENTITY文件存放路径',
    `MAPPER_PACKAGE`       VARCHAR(20) NOT NULL COMMENT 'MAPPER文件存放路径',
    `MAPPER_XML_PACKAGE`   VARCHAR(20) NOT NULL COMMENT 'MAPPER XML文件存放路径',
    `SERVICE_PACKAGE`      VARCHAR(20) NOT NULL COMMENT 'SERVICE文件存放路径',
    `SERVICE_IMPL_PACKAGE` VARCHAR(20) NOT NULL COMMENT 'SERVICEIMPL文件存放路径',
    `CONTROLLER_PACKAGE`   VARCHAR(20) NOT NULL COMMENT 'CONTROLLER文件存放路径',
    `IS_TRIM`              VARCHAR(2)     NOT NULL COMMENT '是否去除前缀 1是 0否',
    `TRIM_VALUE`           VARCHAR(10) NULL DEFAULT NULL COMMENT '前缀内容',
    PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '代码生成配置表'
  ROW_FORMAT = Dynamic;



